#start cleanup script and big runner
nohup git/jgi-metaq/bin/metaq-cleanup.py >|metaq-cleanup.o 2>|metaq-cleanup.e &
nohup git/jgi-metaq/bin/metaq-runner.py -r aws-120-12h -s 172.31.17.117 >|aws-120-12h.o 2>|aws-120-12h.e &

#start local
nohup $METAQ/metaq-runner.py >| local.o 2>| local.e &

#start watcher
nohup git/jgi-metaq/bin/metaq-watch.py >|./watch/watch.o 2>|./watch/watch.e &

#submit run
$METAQ/metaq-submit.py test.json

#restart failed
#$METAQ/metaq-restart.py -t 5db3557c1ca385da50b812d1:stop_task --cmd "/bin/true" --runner local
