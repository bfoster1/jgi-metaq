#!/usr/bin/env python

import os, sys
import argparse
import boto3
import json
from bson import json_util


def main():
     '''main'''
     parser = argparse.ArgumentParser(description=main.__doc__)
     parser.add_argument("-t", "--tag", required=True, help="tag to launch\n")
     parser.add_argument("-i", "--instance", required=True, help="instance type to launch\n")
     parser.add_argument("-e", "--ebs_size", required=False, default='125', help="instance type to launch\n")     
     args = parser.parse_args()
     
     user_data='Q29udGVudC1UeXBlOiBtdWx0aXBhcnQvbWl4ZWQ7IGJvdW5kYXJ5PSIvLyIKTUlNRS1WZXJzaW9uOiAxLjAKCi0tLy8KQ29udGVudC1UeXBlOiB0ZXh0L2Nsb3VkLWNvbmZpZzsgY2hhcnNldD0idXMtYXNjaWkiCk1JTUUtVmVyc2lvbjogMS4wCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQKQ29udGVudC1EaXNwb3NpdGlvbjogYXR0YWNobWVudDsgZmlsZW5hbWU9ImNsb3VkLWNvbmZpZy50eHQiCgojY2xvdWQtY29uZmlnCmNsb3VkX2ZpbmFsX21vZHVsZXM6Ci0gW3NjcmlwdHMtdXNlciwgYWx3YXlzXQoKLS0vLwpDb250ZW50LVR5cGU6IHRleHQveC1zaGVsbHNjcmlwdDsgY2hhcnNldD0idXMtYXNjaWkiCk1JTUUtVmVyc2lvbjogMS4wCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQKQ29udGVudC1EaXNwb3NpdGlvbjogYXR0YWNobWVudDsgZmlsZW5hbWU9InVzZXJkYXRhLnR4dCIKCiMhL2Jpbi9iYXNoCnN1ZG8geXVtIHVwZGF0ZSAteTtzdWRvIHl1bSBpbnN0YWxsIGRvY2tlciBjdXJsIGdpdCBlbWFjcyAteQpjdXJsIC0tc2lsZW50IC0tZmFpbCAtbyBtaW4uc2ggLUwgaHR0cHM6Ly9yZXBvLmNvbnRpbnV1bS5pby9taW5pY29uZGEvTWluaWNvbmRhMy1sYXRlc3QtTGludXgteDg2XzY0LnNoICYmIGJhc2ggbWluLnNoIC1iIC1wICRQV0QvbWluaWNvbmRhMyAmJiBybSBtaW4uc2gKc291cmNlIG1pbmljb25kYTMvZXRjL3Byb2ZpbGUuZC9jb25kYS5zaCAmJiBjb25kYSBhY3RpdmF0ZQpjb25kYSBpbnN0YWxsIC1jIGNvbmRhLWZvcmdlIG15c3FsIHB5bW9uZ28gcGlrYSBhd3NjbGkgLXkKbWtkaXIgZ2l0OyBjZCBnaXQ7IGdpdCBjbG9uZSBodHRwczovL2dpdGxhYi5jb20vYmZvc3RlcjEvamdpLW1ldGFxLmdpdDsgY2QgLi47CnRhZz0kKGF3cyBlYzIgZGVzY3JpYmUtdGFncyAtLXJlZ2lvbj11cy13ZXN0LTIgLS1maWx0ZXJzICJOYW1lPXJlc291cmNlLWlkLFZhbHVlcz0kKGN1cmwgLXMgaHR0cDovLzE2OS4yNTQuMTY5LjI1NC9sYXRlc3QvbWV0YS1kYXRhL2luc3RhbmNlLWlkKSIgfCBqcSAtciAuVGFnc1tdLlZhbHVlKQpub2h1cCBnaXQvamdpLW1ldGFxL2Jpbi9tZXRhcS1jbGVhbnVwLnB5ID58bWV0YXEtY2xlYW51cC5vIDI+fG1ldGFxLWNsZWFudXAuZSAmCm5vaHVwIGdpdC9qZ2ktbWV0YXEvYmluL21ldGFxLXJ1bm5lci5weSAtciAkdGFnIC1zIDE3Mi4zMS4xNy4xMTcgPnwgJHRhZy5vIDI+fCAgJHRhZy5lICYKCi0tLy8K'
     i3_user_data = 'Q29udGVudC1UeXBlOiBtdWx0aXBhcnQvbWl4ZWQ7IGJvdW5kYXJ5PSIvLyIKTUlNRS1WZXJzaW9uOiAxLjAKCi0tLy8KQ29udGVudC1UeXBlOiB0ZXh0L2Nsb3VkLWNvbmZpZzsgY2hhcnNldD0idXMtYXNjaWkiCk1JTUUtVmVyc2lvbjogMS4wCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQKQ29udGVudC1EaXNwb3NpdGlvbjogYXR0YWNobWVudDsgZmlsZW5hbWU9ImNsb3VkLWNvbmZpZy50eHQiCgojY2xvdWQtY29uZmlnCmNsb3VkX2ZpbmFsX21vZHVsZXM6Ci0gW3NjcmlwdHMtdXNlciwgYWx3YXlzXQoKLS0vLwpDb250ZW50LVR5cGU6IHRleHQveC1zaGVsbHNjcmlwdDsgY2hhcnNldD0idXMtYXNjaWkiCk1JTUUtVmVyc2lvbjogMS4wCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQKQ29udGVudC1EaXNwb3NpdGlvbjogYXR0YWNobWVudDsgZmlsZW5hbWU9InVzZXJkYXRhLnR4dCIKCiMhL2Jpbi9iYXNoCmVjaG8gInNldHRpbmcgdXAgbnZtZSIKc3VkbyBmaWxlIC1zIC9kZXYvbnZtZTBuMQpzdWRvIG1rZnMgLXQgeGZzIC9kZXYvbnZtZTBuMQpzdWRvIG1rZGlyIC9zY3JhdGNoCnN1ZG8gbW91bnQgL2Rldi9udm1lMG4xIC9zY3JhdGNoCmNkIC9zY3JhdGNoCnN1ZG8geXVtIHVwZGF0ZSAteTtzdWRvIHl1bSBpbnN0YWxsIGRvY2tlciBjdXJsIGdpdCBlbWFjcyAteQpjdXJsIC0tc2lsZW50IC0tZmFpbCAtbyBtaW4uc2ggLUwgaHR0cHM6Ly9yZXBvLmNvbnRpbnV1bS5pby9taW5pY29uZGEvTWluaWNvbmRhMy1sYXRlc3QtTGludXgteDg2XzY0LnNoICYmIGJhc2ggbWluLnNoIC1iIC1wICRQV0QvbWluaWNvbmRhMyAmJiBybSBtaW4uc2gKc291cmNlIG1pbmljb25kYTMvZXRjL3Byb2ZpbGUuZC9jb25kYS5zaCAmJiBjb25kYSBhY3RpdmF0ZQpjb25kYSBpbnN0YWxsIC1jIGNvbmRhLWZvcmdlIG15c3FsIHB5bW9uZ28gcGlrYSBhd3NjbGkgLXkKbWtkaXIgZ2l0OyBjZCBnaXQ7IGdpdCBjbG9uZSBodHRwczovL2dpdGxhYi5jb20vYmZvc3RlcjEvamdpLW1ldGFxLmdpdDsgY2QgLi47CnRhZz0kKGF3cyBlYzIgZGVzY3JpYmUtdGFncyAtLXJlZ2lvbj11cy13ZXN0LTIgLS1maWx0ZXJzICJOYW1lPXJlc291cmNlLWlkLFZhbHVlcz0kKGN1cmwgLXMgaHR0cDovLzE2OS4yNTQuMTY5LjI1NC9sYXRlc3QvbWV0YS1kYXRhL2luc3RhbmNlLWlkKSIgfCBqcSAtciAuVGFnc1tdLlZhbHVlKQpub2h1cCBnaXQvamdpLW1ldGFxL2Jpbi9tZXRhcS1jbGVhbnVwLnB5ID58bWV0YXEtY2xlYW51cC5vIDI+fG1ldGFxLWNsZWFudXAuZSAmCm5vaHVwIGdpdC9qZ2ktbWV0YXEvYmluL21ldGFxLXJ1bm5lci5weSAtciAkdGFnIC1zIDE3Mi4zMS4xNy4xMTcgPnwgJHRhZy5vIDI+fCAgJHRhZy5lICYKCi0tLy8K'

     if args.instance.startswith('i3'):
          user_data = i3_user_data
     #userdata with cd cromwell_root
     user_data='Q29udGVudC1UeXBlOiBtdWx0aXBhcnQvbWl4ZWQ7IGJvdW5kYXJ5PSIvLyIKTUlNRS1WZXJzaW9uOiAxLjAKCi0tLy8KQ29udGVudC1UeXBlOiB0ZXh0L2Nsb3VkLWNvbmZpZzsgY2hhcnNldD0idXMtYXNjaWkiCk1JTUUtVmVyc2lvbjogMS4wCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQKQ29udGVudC1EaXNwb3NpdGlvbjogYXR0YWNobWVudDsgZmlsZW5hbWU9ImNsb3VkLWNvbmZpZy50eHQiCgojY2xvdWQtY29uZmlnCmNsb3VkX2ZpbmFsX21vZHVsZXM6Ci0gW3NjcmlwdHMtdXNlciwgYWx3YXlzXQoKLS0vLwpDb250ZW50LVR5cGU6IHRleHQveC1zaGVsbHNjcmlwdDsgY2hhcnNldD0idXMtYXNjaWkiCk1JTUUtVmVyc2lvbjogMS4wCkNvbnRlbnQtVHJhbnNmZXItRW5jb2Rpbmc6IDdiaXQKQ29udGVudC1EaXNwb3NpdGlvbjogYXR0YWNobWVudDsgZmlsZW5hbWU9InVzZXJkYXRhLnR4dCIKCiMhL2Jpbi9iYXNoCnN1ZG8geXVtIHVwZGF0ZSAteTtzdWRvIHl1bSBpbnN0YWxsIGRvY2tlciBjdXJsIGdpdCBlbWFjcyAteQojZWNobyAiZWJzLWF1dG9zY2FsZSIKI2NkIC9vcHQgJiYgd2dldCBodHRwczovL3MzLmFtYXpvbmF3cy5jb20vYXdzLWdlbm9taWNzLXdvcmtmbG93cy9hcnRpZmFjdHMvYXdzLWVicy1hdXRvc2NhbGUudGd6ICYmIHRhciAteHpmIGF3cy1lYnMtYXV0b3NjYWxlLnRnegojc2ggL29wdC9lYnMtYXV0b3NjYWxlL2Jpbi9pbml0LWVicy1hdXRvc2NhbGUuc2ggLyAvZGV2L3NkYyAgMj4mMSA+IC92YXIvbG9nL2luaXQtZWJzLWF1dG9zY2FsZS5sb2cKY2QgL2Nyb213ZWxsX3Jvb3QKY3VybCAtLXNpbGVudCAtLWZhaWwgLW8gbWluLnNoIC1MIGh0dHBzOi8vcmVwby5jb250aW51dW0uaW8vbWluaWNvbmRhL01pbmljb25kYTMtbGF0ZXN0LUxpbnV4LXg4Nl82NC5zaCAmJiBiYXNoIG1pbi5zaCAtYiAtcCAkUFdEL21pbmljb25kYTMgJiYgcm0gbWluLnNoCnNvdXJjZSBtaW5pY29uZGEzL2V0Yy9wcm9maWxlLmQvY29uZGEuc2ggJiYgY29uZGEgYWN0aXZhdGUKY29uZGEgaW5zdGFsbCAtYyBjb25kYS1mb3JnZSBteXNxbCBweW1vbmdvIHBpa2EgYXdzY2xpIC15Cm1rZGlyIGdpdDsgY2QgZ2l0OyBnaXQgY2xvbmUgaHR0cHM6Ly9naXRsYWIuY29tL2Jmb3N0ZXIxL2pnaS1tZXRhcS5naXQ7IGNkIC4uOwp0YWc9JChhd3MgZWMyIGRlc2NyaWJlLXRhZ3MgLS1yZWdpb249dXMtd2VzdC0yIC0tZmlsdGVycyAiTmFtZT1yZXNvdXJjZS1pZCxWYWx1ZXM9JChjdXJsIC1zIGh0dHA6Ly8xNjkuMjU0LjE2OS4yNTQvbGF0ZXN0L21ldGEtZGF0YS9pbnN0YW5jZS1pZCkiIHwganEgLXIgLlRhZ3NbXS5WYWx1ZSkKbm9odXAgZ2l0L2pnaS1tZXRhcS9iaW4vbWV0YXEtY2xlYW51cC5weSA+fG1ldGFxLWNsZWFudXAubyAyPnxtZXRhcS1jbGVhbnVwLmUgJgpub2h1cCBnaXQvamdpLW1ldGFxL2Jpbi9tZXRhcS1ydW5uZXIucHkgLXIgJHRhZyAtcyAxNzIuMzEuMTcuMTE3ID58ICR0YWcubyAyPnwgICR0YWcuZSAmCgotLS8vCg=='
     
     client = boto3.client('ec2')
     response = client.run_instances(
         BlockDeviceMappings=[
             {
                 'DeviceName': '/dev/xvda',
                 'Ebs': {
                     'DeleteOnTermination': True,
                     'VolumeSize': int(args.ebs_size),
                     'VolumeType': 'gp2',
                 },
             },
         ],
         ImageId='ami-01935a6afb417a275',
         InstanceType=args.instance,
         MaxCount=1,
         MinCount=1,
         KeyName='bf-20190528-west2',
         SecurityGroupIds=[
             'sg-0c53215a6df7253e5',
         ],
          UserData=user_data,
         TagSpecifications=[
             {
                 'ResourceType': 'instance',
                 'Tags': [
                     {
                         'Key': 'Name',
                         'Value': str(args.tag)
                     },
                 ]
             },
         ],
         InstanceMarketOptions={
             'MarketType': 'spot',
             'SpotOptions': {
                 'MaxPrice': '100',
                 'SpotInstanceType': 'one-time',
             }
         },
         
    EbsOptimized=True,
         IamInstanceProfile={
             'Arn': 'arn:aws:iam::080819234838:instance-profile/bf-20190603-uswest2-iam-GenomicsEnvBatchInstanceProfile-K3S0YR4V9ZND'
         },
         InstanceInitiatedShutdownBehavior='terminate',
     )

     url = response['Instances'][0]['PrivateIpAddress']
     cmd = "ssh -o \"StrictHostKeyChecking no\"  -i ~/Downloads/bf-20190528-west2.pem ec2-user@" + url
     print(cmd)
     file = url + ".json"
     with open (file,"w") as f:f.write(json.dumps(response, indent=3, default=json_util.default))
     return 

if __name__ == '__main__':
    main()
