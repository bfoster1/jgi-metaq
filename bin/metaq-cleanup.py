#!/usr/bin/env python

import sys, os
import argparse
import subprocess
import shutil
import time


VERSION = "1.0.0"
def main():
    '''will monitor /cromwell_root and remove dirs where run folder is successful'''
    
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-d", "--directory", required=False, default='./', help="directory to monitor\n")
    parser.add_argument("-t", "--time", required=False, default=30, help="time in seconds to monitor the directory. (default=30)\n")
    parser.add_argument("-l", "--lag", required=False, default=15, help="time in minutes to delay deletion. (default=15)\n")

    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()
        
    if not os.path.exists(args.directory):
        sys.stderr.write(args.directory + " does not exist \n")
        sys.exit(255)

        
    while 1:
        filelist = list()
        for root, dirs, files in os.walk(args.directory):
            for filename in files:
                if filename.endswith("command.bash.rc"):
                    filepath = os.path.join(root,filename)
                    minutes_old = (time.time() - os.path.getmtime(filepath))/60
                    if minutes_old > float(args.lag):
                        filelist.append(filepath)

        for filename in filelist:
            with open(filename) as f:first_line = f.readline().rstrip()
            if first_line != "0":
                continue
            else:
                sys.stdout.write(filename + " return status = 0\n")                                 
            #/cromwell_root/jgi_meta/create_agp/b0cfd423-c877-4fe4-b268-16c02b00714b/-1/1/create_agp-rc.txt
            #/home/ec2-user/5dafd518c0b70d2983620341/spades/command.bash.rc
            dir_to_rm = os.path.dirname(filename)
            cmd = ["rm","-r",dir_to_rm]
            sys.stdout.write(" ".join(cmd));sys.stdout.flush()
            shutil.rmtree(dir_to_rm)
            sys.stdout.write(" ... done\n")
            sys.stdout.flush()
        time.sleep(int(args.time))


if __name__ == '__main__':
    main()
