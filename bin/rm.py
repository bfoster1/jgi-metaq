#!/usr/bin/env python

import sys, os
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib"))
import utils
import argparse
import json
import subprocess
import shutil
import re
import time
import hashlib

#requires an active aws cli if using s3
'''
args
- s3 or local staging dir with default
- time check argument
- outputdir for final files
- send only flag
'''
if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)
if shutil.which('aws') is None:
    sys.stderr.write('awscli not in path or not installed' + "\n")
    sys.exit(1)

VERSION = "1.0.0"
def main():
    '''given a reads inputfile, and a wdl, copy reads to s3 and launch cromwell job 
    wait for completion and copy outputs back
    '''

    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-i", "--id", required=True, help="cromwell succedded id.\n")
    parser.add_argument("-c", "--cromwellhost", required=False,default="54.212.246.70:8000",  help="cromwell host to push job to\n")
    parser.add_argument("-o", "--outputdir", required=False, default = './', help="outputdir for outputs. default=send to cromwell only\n")
    parser.add_argument("--verbose", required=False, default=False,action='store_true', help="verbose mode")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()

        
    if args.outputdir and not os.path.exists(args.outputdir):
        sys.stderr.write(args.outputdir + " does not exist \n")
        sys.exit(255)
    args.outputdir == os.path.abspath(args.outputdir)


    cmd = ['curl', '-Is', '--max-time', '10', 'http://' + args.cromwellhost ]
    result = utils.run_process(cmd,verbose=False)

    # get workflow metadata
    sub_id = args.id

    cmd = 'curl --silent -X GET "http://' + args.cromwellhost + '/api/workflows/v1/' + sub_id + '/metadata?expandSubWorkflows=false" -H  "accept: application/json"'
    if args.verbose: sys.stdout.write("cromwell_check: " + cmd + "\n");sys.stdout.flush()    
    result = utils.run_process(cmd,verbose=False)
    metadata = json.loads(result['stdout'])
    if metadata['status'] !="Succeeded":
        sys.stderr.write(args.id + " does not have status=Succeeded: " + metadata['status'] + "\n")
        sys.exit(1)        

    dirs = dict()
    if len(metadata['outputs']):
        dirs = copy_outputs(metadata, args.outputdir, args.verbose)
        outputfile=os.path.join(os.path.dirname(dirs['local_dir']),"dirname.txt")
        with open(outputfile,"w") as f:
            f.write(dirs['local_dir'] + "\n")
        cmd = ["aws","s3", "rm", dirs['s3_dir'], "--recursive"]
        result = utils.run_process(cmd,verbose=args.verbose)        
    else:
        sys.stderr.write("Error no outputs found in cromwell metadata\n")
        sys.exit(1)

    inputs_file_s3 = ""
    if "submittedFiles" in metadata and metadata['submittedFiles']['inputs'].find('jgi_meta.input_file') != -1:
        inputs_json = json.loads(metadata['submittedFiles']['inputs'])
        inputs_file_s3 = inputs_json['jgi_meta.input_file']

    if inputs_file_s3:
        cmd = ["aws","s3", "rm", inputs_file_s3]
        result = utils.run_process(cmd,verbose=args.verbose)

    sys.exit(0)
        

def copy_outputs(metadata, localdir, verbose=False):
    '''takes cromwell metadata, finds the first s3 output and cps it locally with prefix                                                                                                                 if successfull, returns the local dir and s3 dir for deletion'''

    s3_dir = ""
    cromwellid = metadata['id']
    prefix = ""
    if "submittedFiles" in metadata and metadata['submittedFiles']['inputs'].find('jgi_meta.input_file') != -1:
        inputs_json = json.loads(metadata['submittedFiles']['inputs'])
        if 'prefix' in inputs_json:
            prefix = inputs_json['prefix'] + "." + cromwellid
    else:
        prefix = str(time.time()) + "." + cromwellid

    localdir_root= os.path.join(localdir,prefix)

    #to add, check for outputs. If not, then get source path from "calls"                                                                                                                              
    try:
        for key in sorted(metadata['outputs']):
            if s3_dir:
                continue
            source_path = metadata['outputs'][key]
            print (str(source_path))
            if not str(source_path).startswith('s3://'):
                continue
            if source_path.find('/call-') != -1 and source_path.find('/cromwell-execution/') != -1:
                s3_dir = re.sub(r'^(.*?/)call-.*$',r'\1',source_path)
#            cmd = ["aws","s3", "cp", s3_dir, localdir_root, "--recursive"]
#            if verbose:sys.stdout.write(" ".join(cmd))
#            procExe = subprocess.Popen(cmd , stdout=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
#            while procExe.poll() is None:
#                line = procExe.stdout.readline()
#                if verbose and line.startswith("download:"):
#                    sys.stdout.write(line)
    except Exception as e:
        sys.stderr.write(e)
        return
    return {"s3_dir":s3_dir,"local_dir":localdir_root}



if __name__ == '__main__':
    main()
