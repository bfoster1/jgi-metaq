#!/usr/bin/env python

import os, sys
import argparse
import boto3
import json
from bson import json_util


def main():
     '''main'''
     parser = argparse.ArgumentParser(description=main.__doc__)
     parser.add_argument("-t", "--tag", required=True, help="tag to launch\n")
     parser.add_argument("-i", "--instance", required=True, help="instance type to launch\n")
     args = parser.parse_args()

     client = boto3.client('ec2')
     response = client.run_instances(
         BlockDeviceMappings=[
             {
                 'DeviceName': '/dev/xvda',
                 'Ebs': {
                     'DeleteOnTermination': True,
                     'VolumeSize': 123,
                     'VolumeType': 'gp2',
                 },
             },
         ],
         ImageId='ami-01935a6afb417a275',
         InstanceType=args.instance,
         MaxCount=1,
         MinCount=1,
         KeyName='bf-20190528-west2',
         SecurityGroupIds=[
             'sg-0c53215a6df7253e5',
         ],
         TagSpecifications=[
             {
                 'ResourceType': 'instance',
                 'Tags': [
                     {
                         'Key': 'Name',
                         'Value': str(args.tag)
                     },
                 ]
             },
         ],
         InstanceMarketOptions={
             'MarketType': 'spot',
             'SpotOptions': {
                 'MaxPrice': '100',
                 'SpotInstanceType': 'one-time',
             }
         },
         
    EbsOptimized=True,
         IamInstanceProfile={
             'Arn': 'arn:aws:iam::080819234838:instance-profile/bf-20190603-uswest2-iam-GenomicsEnvBatchInstanceProfile-K3S0YR4V9ZND'
         },
         InstanceInitiatedShutdownBehavior='terminate',
     )

     print(json.dumps(response, indent=3, default=json_util.default))
     return 

if __name__ == '__main__':
    main()
