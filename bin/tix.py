#!/usr/bin/env python

import dateutil.parser
import datetime
import re
import sys
import getpass
import base64
from jira.client import JIRA
import json
import argparse
import collections
import numbers
from pytz import timezone
import datetime
#utc=pytz.UTC
pst = timezone('US/Pacific')


#Ticket breakdown by:
# G metaG
# T metaT
# I Improved Draft MetaG
# C Crashes
# 

# last comment by other

def hasher():
    return collections.defaultdict(hasher)

VERSION="1.0.0"
def main():
    parser = argparse.ArgumentParser(description='Get jira tickets.')
    parser.add_argument("option", default="", nargs='?', type = str, help="Index of option to report.")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " +  VERSION + ")")
    parser.add_argument("-c", "--comments_outstanding", default=False,
                        required=False, action='store_true', help="[Optional] only return outstanding comments\n")
    parser.add_argument("-w", "--with_comments", default=False,
                        required=False, action='store_true', help="[Optional] only return tickets with comments\n")
    parser.add_argument("-m", "--with_mentions", default=False,
                        required=False, action='store_true', help="[Optional] only return tickets with mentions\n")
    parser.add_argument("-d", "--days_since", nargs='?', const=30, type=int, help="[Optional] only return tickets updated longer than (default all, -p flag no args=30)\n")
#    parser.add_argument("-d", "--debug",
#                        required=False, default=False, help="[Optional] print debugging information\n")
    args = parser.parse_args()
    up = 'WW1admMzUmxjZz09OlFXSjJObXRoZFdrPQ=='
    jira = JIRA(options={'server': 'https://issues.jgi-psf.org'},basic_auth=(base64.b64decode(base64.b64decode(up).decode('utf-8').split(":")[0]),base64.b64decode(base64.b64decode(up).decode('utf-8').split(":")[1])))

    option = args.option
    if bool(re.match('^\d+$',option)) or option == "" :
        pass
    else:
        sys.exit("option must be blank or number")


    filter = 'assignee = currentUser() AND resolution = Unresolved order by updated DESC'
    issues = jira.search_issues(filter,maxResults=500,fields = ['comment','reporter','summary','description','created'])
    issue_dict = hasher()
    for issue in issues:
        keep=True

        if args.with_comments:
            if len(issue.fields.comment.comments) <1:
                keep=False

        #discard if there are no comments or last comment mine
        if args.comments_outstanding:
            if len(issue.fields.comment.comments) <1:
                keep=False
            elif issue.fields.comment.comments[-1].author.name == 'bfoster':
                keep=False

        #discard if there are no mentions
        if args.with_mentions:
            if len(issue.fields.comment.comments) <=1:
                keep=False
            mentions = 0
            for com in issue.fields.comment.comments:
                if com.body.find('[~bfoster]') !=-1:
                    mentions+=1
            if mentions==0:
                keep=False


        #toss if newer than args.days_since days
        if args.days_since != None:
            created = issue.fields.created
            created = dateutil.parser.parse(created)
            if created.tzinfo == None:
                created = pst.localize(created)

            today = str(datetime.datetime.now())
            today = dateutil.parser.parse(today)
            if today.tzinfo == None:
                today = pst.localize(today)
            diff = today - created
            if diff.days <= args.days_since:
                keep=False

        if keep==False:
            continue
        issue_dict["all"][issue.key] = issue

        if issue.fields.reporter.name == "jamo":
            if issue.fields.summary.find("Metagenome") != -1 and issue.fields.summary.find("Improved") == -1:
                issue_dict["metaG"][issue.key] = issue
            elif issue.fields.summary.find("Metatranscriptome") != -1:
                issue_dict["metaT"][issue.key]=issue
            else:
                issue_dict["misc_jamo"] = issue
        elif issue.fields.summary.find("Metagenome") != -1 and issue.fields.summary.find("Improved") != -1:
            issue_dict["metaI"][issue.key] = issue
        elif issue.fields.summary.find("ail") != -1:
            issue_dict["failures"][issue.key] = issue 
        else:
            issue_dict["misc"][issue.key] = issue 

            
    sorted_keys = sorted(issue_dict.keys())
    subtask_list = dict()
    if option == "":
        for i in range(1,len(sorted_keys)+1):
            type = sorted_keys[i-1]
            print(str(i) + ") " + type + "\t\t" + str(len(issue_dict[type])))
    elif bool(re.match('^\d+$',option))  and (int(option) > 0 and int(option) <= (len(sorted_keys)+1)):
        option = str(int(option) - 1 ) 
        if sorted_keys[int(option)] in list(issue_dict.keys()):
            for issue in sorted(issue_dict[sorted_keys[int(option)]]):
                i = issue_dict[sorted_keys[int(option)]][issue]
                #print issue + "\t" + i.fields.summary + "\t" + get_libs_from_issue(i) + "\t"  +  i.fields.created
                #print(issue + "\t" + i.fields.summary + "\t" + get_libs_from_issue(i)) 
                lines = i.fields.description.splitlines()
                for line in lines:
#                    import pdb;pdb.set_trace()
                    if line.find('fastq.gz') !=-1 :                    
                        print(issue + "\t" + i.fields.summary + "\t" + line)
#                import pdb;pdb.set_trace()
#                if len(lines)==2:
#                    if bool(re.match('^.*\d+\.fastq.gz.*',lines[-1])):
#                        continue
                    
#                    lib = lines[-1].split(", ")[-2]
#                    print(issue + "\t" + i.fields.summary + "\t" + lib)
#                    if lines[-1].find('12859.3.292495.fastq.gz') !=-1:
#                        import pdb;pdb.set_trace()                        

    else:
        sys.exit("Invalid option\n")

    sys.exit()


def get_libs_from_issue(issue):
    library = ""
    multi = isIssueMulti(issue)
    #for standard assembly
    if multi: # libraries are in the description body
        libraries = dict()
        for line in issue.fields.description.splitlines():
            result = re.search('\s*(\d{7,7}).*\s([A-Z]{4,5}).*',line)
            if hasattr(result,'group'):
                if result.group(1) and result.group(2):
                    libraries[result.group(2)] = 1
        library = ",".join(list(libraries.keys()))
                        
    else: # library is in the summary at the end
        line = issue.fields.summary.rstrip()
        result = re.search('.*?\b([A-Z]{4,5})$',line)
        if hasattr(result,'group'):
            if result.group(1):
                library = result.group(1)

    return library

def isIssueMulti(iss):
    '''split ticket will have pool in the title and 
    a line with "Library" in the the description'''
    split_ticket = False
    if ("ool" in iss.fields.summary): #potential split
        for line in iss.fields.description.splitlines():
            if ("ibrary" in line):
                split_ticket = True
    return split_ticket

if __name__ == "__main__":
    main()


#    parser = argparse.ArgumentParser(description='Split multiple tickets into Sub-tasks')
#    parser.add_argument("-e", "--exe", help="execute jira split ticket.",action="store_true")
#    args = parser.parse_args()
#    sys.stderr.write("Username:")
#    u=raw_input()
#    p=getpass.getpass()
