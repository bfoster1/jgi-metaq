#!/usr/bin/env python

import json
import subprocess

cmd = 'cromwell_jobs.bash'
jobs = json.loads(subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read())

ids = list()
for result in jobs['results']:
    if result["status"]== "Succeeded":
        ids.append(result["id"])

for _id in ids:
    cmd = "cromwell_metadata.bash.all.bash " + _id
    json=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8')
    if json.find("12999.1.303797.GGACTCCT-CTAAGCCT.filter-METAGENOME.fastq.gz") !=-1 and json.find("runner.201910") !=-1:
        print(_id)
        

    
