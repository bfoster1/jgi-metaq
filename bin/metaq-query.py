#!/usr/bin/env python

import sys,os
from pymongo import MongoClient
from bson.objectid import ObjectId
#from bson.json_util import dumps
#import pprint
import json

_id=""
if len(sys.argv)==2:
    _id=sys.argv[1]
elif len(sys.argv)==1:
    _id=""
else:
    sys.exit("0 or 1 args expected")
    
url = 'mongodb://172.31.17.117:27017'
client = MongoClient(url)
db = client.workflows
workflows=db.workflows

cursor = db.workflows
for document in cursor.find():
    document['_id']=str(document['_id'])
    if _id and document['_id']==_id:
        print(json.dumps(document,indent=3))
    elif not _id:
        print(json.dumps(document,indent=3))
        

#ids = ["5d93e0a6d038270ff4964521","5d93e0afe893787949d6ba2a"]          
#for _id in ids:
#     flow = workflows.delete_one({'_id': ObjectId(_id) })
#     flow['_id']=str(flow['_id'])
#     print(json.dumps(flow,indent=3))
