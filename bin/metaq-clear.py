#!/usr/bin/env python

import sys,os
import json
import glom
import re
import time
from pymongo import MongoClient
from bson.objectid import ObjectId
import pika
from time import sleep

''' update client to be like metaq-add.py vars'''
url = '54.212.246.70'
url = '172.31.17.117'
client = MongoClient('mongodb://' + url + ':27017')
db = client.workflows
mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host=url,heartbeat=600,blocked_connection_timeout=300))
channel = mq_connection.channel()
channel.queue_delete(queue='tasks')
channel.queue_delete(queue='local')
channel.queue_delete(queue='aws-120-12h')
channel.queue_delete(queue='aws-240-24h')
channel.queue_delete(queue='remote')
channel.queue_delete(queue='asdf')
channel.queue_delete(queue='aws-remote')
     
