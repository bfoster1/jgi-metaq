#!/usr/bin/env python

import os,sys
import time
from time import sleep
import pika
import json
from pymongo import MongoClient
from bson.objectid import ObjectId
import subprocess
import argparse
import re
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import utils

def main():
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-r", "--runnername", required=False, default="local", help="runner name default local\n")
    parser.add_argument("-s", "--server", required=False, default='54.212.246.70', help="server for mongodb and rabbitmq connections\n")
    args = parser.parse_args()
    

    client = MongoClient('mongodb://' + args.server + ':27017')
    global db
    db = client.workflows

    mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host=args.server,heartbeat=600,blocked_connection_timeout=300))    
    channel = mq_connection.channel()
    channel.queue_declare(queue=args.runnername, durable=True)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=args.runnername, on_message_callback=callback)
    print('Waiting for messages. To exit press CTRL+C');sys.stdout.flush()
    while 1:
        try:
            channel.start_consuming()
        except:
            print("start_consuming failed ... restarting")
            mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host=args.server,heartbeat=600,blocked_connection_timeout=300))    
            channel = mq_connection.channel()
            channel.queue_declare(queue=args.runnername, durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(queue=args.runnername, on_message_callback=callback)
            continue
        sleep(10)
        


def callback(ch, method, properties, body):
#    ch.basic_ack(delivery_tag=method.delivery_tag) #auto ack to prevent requeues
#    import pdb;pdb.set_trace()
    ch.basic_ack(delivery_tag=method.delivery_tag)
    task=json.loads(body)
    if '_id' not in task:
        sys.stdout.write("No id in task " + body + "\n")
        return
    if task['_id'].find(":")==-1:
        sys.stdout.write("Malformed task should have wfid\":\"taskname " + task['_id'] + "\n")
        return
    _id = task['_id'].split(':')[0]
    task_name = re.sub(_id + ":",r'',task['_id'])
    document = db.workflows.find_one({"_id": ObjectId(_id)})
    if not(document):
        sys.stderr.write("no doc for " + _id)
        return
    cmd = document[task_name]['cmd'][-1]

    run_dir = ""
    cd_cmd = cmd[0]
    if not cd_cmd.startswith("cd "):
        sys.stdout.write("command list should start with cd in to task level run dir  " + cd_cmd + "\n")
        utils.update_status(db, str(_id), taskname=task_name,status='Error')
        return
    else:
        run_dir=re.sub(r'cd\s+',r'',cd_cmd)
    if not os.path.exists(run_dir):
        os.makedirs(run_dir)
        
    with open(os.path.join(run_dir,"task.json"), "w") as f: f.write(json.dumps(task,indent=3))
    for filename in task['file']:
        if filename.startswith('s3out'):
            if not task['file'][filename].startswith('./'):
                cmd_result=utils.run_process("echo \"malformed s3 filename\" && /bin/false",fail=False)
            local_copy = os.path.join(run_dir,os.path.dirname(task['file'][filename]))
            if not os.path.exists(local_copy):
                os.makedirs(local_copy)
                
    bashfile=write_bash(cmd=cmd,run_dir=run_dir,bashfile="command.bash")

    utils.update_status(db, str(_id), taskname=task_name,status='Running')
    print(task['_id'] + " Running");sys.stdout.flush()
    cmd = "bash " + bashfile
    cmd +=  " 1>| " + bashfile + ".o" + " "
    cmd +=  " 2>| " + bashfile + ".e" 
    cmd_result=utils.run_process(cmd,fail=False)

    with open(bashfile + ".rc", "w") as f:
        f.write(str(cmd_result['rc']) + "\n")
        
    if cmd_result['rc'] ==0:
        utils.update_status(db, str(_id), taskname=task_name,status='Done')
        print(task['_id'] + " Done");sys.stdout.flush()
    else:
        utils.update_status(db, str(_id), taskname=task_name,status='Error')
        print(task['_id'] + " Error");sys.stdout.flush()

def write_bash(directive_list=[], cmd=[], run_dir="", bashfile=""):
    '''write_bash
    inputs: directive list: for cluster  ... etc
            cmd: command list
            run_dir: run folder
            bashfile: where to put the file
    returns:
            bashfile
    '''
    outfile = os.path.join(run_dir,bashfile)
    with open(outfile, "w") as f:
        f.write('#!/usr/bin/env bash' + "\n\n")
        f.write("ulimit -c 0\numask 002\nset -eo pipefail\n\n")
#        f.write("cd " + os.path.realpath(run_dir) + "\n\n")

        if len(directive_list)>0:
            f.write("\n".join(directive_list) + "\n\n")

        for cmd_string in cmd:
 #           if cmd_string.find(">") ==-1:#only collect stderr and stdout if no redirect
 #               cmd_string +=  " 1>> command.bash.o 2>> command.bash.e\n\n"
 #           else:
 #               cmd_string += "\n\n"
            cmd_string += "\n\n"
            f.write(cmd_string)
            
    os.chmod(outfile, 0o775)
    return outfile

def run_process(cmd, verbose=False, fail=True):
    cmd_string=""
    if isinstance(cmd,list):
        if verbose:
            sys.stdout.write("call:" + " ".join(cmd) + " ")
        out = subprocess.Popen(cmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        cmd_string = " ".join(cmd)
    elif isinstance(cmd,str):
        if verbose:
            sys.stdout.write("call:" + cmd + " ")
        out = subprocess.Popen(cmd,shell=True,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        cmd_string = cmd
    else:
        sys.stderr.write("trouble running:" + str(cmd), "\n")
        sys.exit(1)
    comm = out.communicate()
    stdout = comm[0].decode('utf-8') if comm[0] is not None else ""
    stderr = comm[1].decode('utf-8') if comm[1] is not None else ""
    rc = out.returncode
    if rc !=0 and fail:
        sys.stderr.write("Trouble running: \"" + cmd_string + "\"\n")
        sys.stderr.write("Error code:" + str(rc) + "\n")
        sys.exit(rc)
    elif verbose:
        sys.stdout.write(" ... returned: " + str(rc) + "\n")
    return_vals = {"stdout":stdout, "stderr":stderr, "rc": rc, "cmd": cmd}
    return return_vals




if __name__ == '__main__':
    main()

