#!/usr/bin/env python

import sys,os
import json
import re
import time
from pymongo import MongoClient
import pymongo
from bson.objectid import ObjectId
import pika
import argparse
from time import sleep
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import utils

''' update client to be like metaq-add.py vars'''
#url = '54.212.246.70'
def main():
     '''main'''
     parser = argparse.ArgumentParser(description=main.__doc__)
     parser.add_argument("-t", "--taskid", required=True, default='localhost', help="task id example 5db33327b9b6e20e25b2411a:assy \n")
     parser.add_argument("-c", "--cmd", required=False, help="optional command string to append\n")
     parser.add_argument("-r", "--runner", required=False, help="optional change runner\n")               
     parser.add_argument("-s", "--server", required=False, default='172.31.17.117', help="server for mongodb and rabbitmq connections\n")
     parser.add_argument("--status", required=False, default='Initiated', help="change status to this\n")
     args = parser.parse_args()

     #mongodb
     client = MongoClient('mongodb://' + args.server + ':27017')
     db = client.workflows

     if ":" not in args.taskid:
          sys.stderr.write('Malformed task id should look like "5db33327b9b6e20e25b2411a:assy"' + "\n")
          sys.exit()
     
     _id=args.taskid.split(":")[0]
     task = re.sub(_id + ":",r'',args.taskid)

     
     if args.cmd:
          cmdlist = list()
          cmdlist.append(args.cmd)
          workflow = db.workflows.find_one({"_id":ObjectId(_id)})
          cmds = workflow[task]["cmd"]
          cmds_to_push = [workflow[task]["cmd"][-1][0]] + [args.cmd]
          result = db.workflows.find_one_and_update({"_id":ObjectId(_id)}, {'$push':{task+'.cmd':cmds_to_push}})
     if args.runner:
          result = db.workflows.find_one_and_update({"_id":ObjectId(_id)}, {'$set':{task+'.runner':args.runner}})          
          
     utils.update_status(db, str(_id),taskname=task, status=args.status)
     utils.update_status(db, str(_id), status=args.status)
     return

if __name__=='__main__':
     main()
