#!/usr/bin/env python

import sys,os
import json
import glom
import re
import time
from pymongo import MongoClient
from bson.objectid import ObjectId
import argparse

sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import utils

def main():
    '''Takes a json input file wf and adds to mongodb. Flesing out the details along the way.'''
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('inputfile', help="input json file operate on")
    parser.add_argument("-d", "--dryrun", required=False, default=False, action="store_true", help="print run json, then exit \n")  
    parser.add_argument("-m", "--mongourl", required=False, default='mongodb://54.212.246.70:27017', help="output dir to place the file. default = cwd\n")
    args = parser.parse_args()

    if not os.path.exists(args.inputfile) or not os.path.isfile(args.inputfile):
        sys.stdout.write(args.inputfile + " does not exist\n")
        sys.exit(125)
    validate_vars(args.inputfile)
    with open(args.inputfile, "r") as f: workflow =json.loads(f.read())
    workflow = initialize_workflow(workflow)

    #add mongo id
    _id =""
    if args.dryrun:
        _id="dryrun"
    else:
        client = MongoClient(args.mongourl)
        db = client.workflows
        workflows=db.workflows
        record = workflows.insert_one(dict()) #emtpy record created
        _id=str(record.inserted_id)
    workflow['_id']=_id

    workflow = expand_containers(workflow)
    workflow = expand_cmd_filepaths(workflow)
    workflow = expand_file_filepaths_s3out(workflow)
    workflow = expand_cmd_filepaths_s3in(workflow)
    workflow = expand_localizebyrunner(workflow)
    workflow = expand_localizebytask(workflow)
    workflow = expand_cleanupall(workflow)
    workflow = expand_cmd_add_cd_rundir(workflow)
    
    if args.dryrun:
        print(json.dumps(workflow,indent=3))
        sys.exit(1)

    del workflow['_id']
    db.workflows.find_one_and_update({"_id": ObjectId(record.inserted_id)}, {"$set": workflow})
    updated = utils.update_status(db, _id, status="Initiated")
    print('{"id":"' + str(record.inserted_id) + '"}')
    return

def expand_cleanupall(workflow):
    '''cleanup, finds all s3 files staged out to delete adds dependencies to all :localize tasks'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    if ":cleanupall:" not in task_names:
        return workflow

    localize_task_list=list()
    for task in task_names:
        if task.startswith(":localize"):
            localize_task_list.append(task)
    workflow[':cleanupall:']['depend']=list(set(workflow[':cleanupall:']['depend']+localize_task_list))
    s3_list = list()
    for task in task_names:
        if "_s3" in workflow[task]:
            for s3_file in workflow[task]['_s3']:
                s3_list.append(s3_file)

    s3_cleanup_cmds = list()
    for s3_file in s3_list:
        s3_cleanup_cmds.append("aws s3 rm " + s3_file)
    workflow[":cleanupall:"]['cmd'].append(s3_cleanup_cmds)
    return workflow

def expand_localizebyrunner(workflow):
    '''takes workflow and exapands :localize: tasks'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]

    runner_to_task=dict()
    for task in task_names:
        runner = workflow[task]['runner']
        if runner not in runner_to_task:
            runner_to_task[runner]=list()
        runner_to_task[runner].append(task)

    for task in task_names:
        if task.startswith(":localizebyrunner:"):
            runner = task.split(":")[-1]
            if runner not in runner_to_task:
                print(runner + " from " + task + " not in runner list . Skipping")
                print(json.dumps(runner_to_task,indent=3));sys.exit()
                continue
            runner_tasks = runner_to_task[runner]
            new_localizebytask_list = list()
            for runner_task in runner_tasks:
                new_task_name = ":localizebytask:" + runner_task
                if new_task_name in workflow:
                    print(new_task_name + " already in workflow. Skipping")
                    continue
                #create new subtask
                new_task = dict()
                new_task = initialize_task(new_task)
                #this new localize sub task has a dependency on the target task.
                #also inherits deps from any given to :localizebyrunner:
                new_task['depend']=list(set([runner_task]+workflow[task]['depend']))
                workflow[new_task_name]=new_task
                new_localizebytask_list.append(new_task_name)
            # :localizebyrunner: will now have dependencies on the newly created :localizebytask:
            workflow[task]['depend']=list(set(workflow[task]['depend'] + new_localizebytask_list))
    return workflow

def expand_localizebytask(workflow):
    '''takes workflow and looks in file paths for :s3: from another task (meaning stage it in)'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        if task.startswith(":localizebytask:"):
            task_to_localize = task.split(":")[-1]
            if task_to_localize not in task_names:
                print(task_to_localize + " from " + task + " not in tasklist")
                continue
            if '_s3' not in workflow[task_to_localize]:
                continue # nothing to be done ie. remote empty placeholder task
            localize_cmds=list()
            for s3 in workflow[task_to_localize]["_s3"]:
                bucket = workflow["_config"]["s3"]
                local_dir = workflow["_config"]["run_dir"]
                local_version=re.sub(bucket, local_dir, s3)
                localize_cmds.append("mkdir -p $(dirname " + local_version + ")")
                localize_cmds.append("aws s3 cp " + s3 + " " + local_version)
            if localize_cmds:
                workflow[task]['cmd'].append(localize_cmds)
    return(workflow)

def expand_localizebydependency(workflow):
    '''takes workflow and exapands :localize: tasks'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        if task.startswith(":localizebydependency:"):
            localize_cmds = list()
            for dep in workflow[task]['depend']:
                for s3 in workflow[dep]['_s3']:
                    bucket = workflow["_config"]["s3"]
                    local_dir = workflow["_config"]["run_dir"]
                    local_version=re.sub(bucket, local_dir, s3)
                    localize_cmds.append("mkdir -p $(dirname " + local_version + ")")
                    localize_cmds.append("aws s3 cp " + s3 + " " + local_version)
            if localize_cmds:
                workflow[task]['cmd'].append(localize_cmds)
    return workflow

        
def expand_cmd_filepaths_s3in(workflow):
    '''takes workflow and looks in file paths for :s3: from another task (meaning stage it in)'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        tmpcmd=list()
        s3_stagein_cmds = list()
        for cmd in workflow[task]['cmd'][-1]:
            variables = re.findall(r'{{(.*?)}}',cmd)
            for var in variables:
                if var.find(":s3:") ==-1:
                    continue
                if var.split(".")[0] ==task: #s3 tag from the same task
                    continue #this is a stage out not stage in

                file_name=glom.glom(workflow,var)
                s3_filepath=""
                if var.split(".")[0].startswith("_config"): #s3 file stored in config, need to stagein with no dependency
                    if not file_name.startswith("s3://"):
                        sys.stdout.write("config s3s should be full path \"s3://\"")
                        sys.exit(255)
                    s3_filepath = file_name
                else:# s3 from a previosly run task in s3 and needs parant_task path
                    parent_task = var.split(".")[0]
                    s3_filepath=os.path.join(workflow["_config"]["s3"],_id, parent_task, file_name)

                localized_s3_filepath=re.sub(r"s3://",r"./",s3_filepath)
                mk_and_cp = "mkdir -p $(dirname " + localized_s3_filepath + ") && aws s3 cp " + s3_filepath + " " + localized_s3_filepath
                s3_stagein_cmds=list(set(s3_stagein_cmds + [mk_and_cp]))
                cmd = re.sub('{{' + var + '}}', localized_s3_filepath , cmd)
            tmpcmd.append(cmd)
        if s3_stagein_cmds:
            workflow[task]['cmd'].append(s3_stagein_cmds + tmpcmd)
    return workflow

def expand_file_filepaths_s3out(workflow):
    '''takes workflow and looks in file paths for :s3: (meaning stage it out)'''
    '''also add touch commands and aux files stage out'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        #gather stageout cmds, files based on file section of task
        s3_stageout_cmds = list()
        for filetag in workflow[task]['file']:
            if filetag.startswith(":s3:"):
                #add ls command for output, add single stage out command, and append s3 file to _s3 section of task
                local_outfile = workflow[task]['file'][filetag]
                s3_filepath=os.path.join(workflow["_config"]["s3"],_id, task, local_outfile)
                #following will fail if not present after task is run
                s3_stageout_cmds.append("ls " + local_outfile + " && aws s3 cp " + local_outfile + " " + s3_filepath)
                if '_s3' not in workflow[task]:
                    workflow[task]['_s3']=list()
                workflow[task]['_s3'].append(s3_filepath)

        #take care of expanding cmds
        tmpcmd=list()
        for cmd in workflow[task]['cmd'][-1]:
            variables = re.findall(r'{{(.*?)}}',cmd)
            for var in variables:
                if var.find(":s3:") ==-1:
                    continue
                if var.split(".")[0] !=task: #s3 tag from a different task
                    continue #this is a stage in not stage out

                file_name=glom.glom(workflow,var)
                cmd = re.sub('{{' + var + '}}', file_name, cmd) 
            tmpcmd.append(cmd)

        aux_file_stageout_cmds=list()
        if s3_stageout_cmds:
            if not workflow[task]['runner'].startswith("local"): #add aux files so they arent left behind
                aux_files = 'command.bash command.bash.o command.bash.e'
                for aux_file in aux_files.split():
                    s3_filepath=os.path.join(workflow["_config"]["s3"],_id, task, aux_file)
                    workflow[task]['_s3'].append(s3_filepath) # add to task s3 list
                    aux_file_stageout_cmds.append("aws s3 cp " + aux_file + " " + s3_filepath)
            workflow[task]['cmd'].append(tmpcmd + s3_stageout_cmds + aux_file_stageout_cmds)
    return workflow
        
def expand_cmd_filepaths(workflow):
    '''takes workflow and expands command file paths (except :s3: or :container:)'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        tmpcmd=list()
        path_found = False
        for cmd in workflow[task]['cmd'][-1]:
            variables = re.findall(r'{{(.*?)}}',cmd)
            for var in variables:
                if var.startswith(":container:") or var.find(":s3:") !=-1: #add check for malformed :: tags
                    continue
                path_found=True
                file_name=glom.glom(workflow,var)
                fields = var.split(".")
                if fields[0] == task or fields[0].startswith("_"):  #this task
                    cmd = re.sub('{{' + var + '}}', file_name, cmd)
                else: #outside task
                    parent_task=fields[0]
                    cmd = re.sub('{{' + var + '}}', os.path.join(workflow["_config"]['run_dir'],_id,parent_task,file_name), cmd)
            tmpcmd.append(cmd)
        if path_found:
            workflow[task]['cmd'].append(tmpcmd)
    return workflow

def expand_cmd_add_cd_rundir(workflow):
    '''takes workflow and adds cd run_dir as first step in workflow'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        tmpcmd=list()
        task_run_dir = ""
        if workflow[task]['runner'].startswith("local"):
            task_run_dir=os.path.join(workflow['_config']['run_dir'],_id,task)
        else:
            task_run_dir=os.path.join(".",_id,task)
        tmpcmd="cd " + task_run_dir
        workflow[task]['cmd'].append([tmpcmd]+ workflow[task]['cmd'][-1])
    return workflow

def initialize_workflow(workflow):
    '''takes a input workflow and adds runner, run_folder, dependencies, status ... etc'''
    if '_config' not in workflow:
        workflow['_config']=dict()
    if 'run_dir' not in workflow['_config']:
        workflow['_config']['run_dir']=os.getcwd()
    task_names = [task for task in workflow if not task.startswith("_")]
    for task_name in task_names:
        if task_name.startswith("_"):
            continue
        task = workflow[task_name]
        task = initialize_task(task)
    #set dependencies
    for task_name in task_names:
        if task_name.startswith("_"):
             continue
        task = workflow[task_name]
        #parse command and set dependencies
        if 'depend' not in task:
            task['depend']=list()
        task['depend'] = get_dependencies(workflow,task_name)
        workflow[task_name] = task #add updated task back to workflow
    
    return workflow

def initialize_task(task):
    #set runner if not there
    if 'runner' not in task:
        task['runner'] = "local"

    #set status if not set
    if '_status' not in task:
        task['_status']=list()
        task['_status'].append({'status':'Initiated','time':time.strftime("%Y-%m-%d %H:%M:%S")})

        #check command and make a list
    if 'cmd' in task:
        tmpcmd = task['cmd']
        if isinstance(tmpcmd,str):
            tmpcmd = [tmpcmd]
        task['cmd']=list()
        task['cmd'].append(tmpcmd)
    else:
        task['cmd']=[[]]

    #file not set
    if 'file' not in task:
        task['file']=dict()
    return task

def update_status_old(db, workflow_id, taskname=None, status="", metadata={}):
    '''Takes db handle, workflow_id,taskname, status, and any other metadata and pushes status to db'''
    if isinstance(workflow_id,str):
        workflow_id=ObjectId(workflow_id)

    if metadata:
        status_update = {'status':status,'metadata':metadata, 'time':time.strftime("%Y-%m-%d %H:%M:%S")}
    else:
        status_update = {'status':status,'time':time.strftime("%Y-%m-%d %H:%M:%S")}

    if taskname is None:  #update workflow status
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{'_status':status_update}})
    else:
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{task_name+'._status':status_update}})

    if not result: #update task status
        sys.stderr("Something wrong with status update\n")
        return False
    return True

def validate_vars(filename):
    with open(filename, "r") as f: workflow_txt =f.read()
#    #config double check s3 bucket
#    if '_config' in workflow and 's3bucket' in workflow['_config']:
#        if not workflow['_config']['s3bucket'].startswith('s3://'):
#            sys.stderr.write("s3bucket looks malformed! sholud have \"s3://\"" + workflow['_config']['s3bucket'] + "\n")
#            sys.exit(277)
#        workflow['_config']['s3bucket_prefix']= os.path.join(workflow['_config']['s3bucket'] , str(workflow['_id']))
    
    workflow_txt = re.sub(r':shifter:',r'',workflow_txt)
    workflow_txt = re.sub(r':docker:',r'',workflow_txt)
    try:
        workflow = json.loads(workflow_txt)
    except:
        sys.stderr.write(filename + " likely not valid json\n");
        sys.exit(1)
    variables = re.findall(r'{{(.*?)}}',workflow_txt)
    for var in variables:
        try:
            glom_test=glom.glom(workflow,var)
        except:
            sys.stderr.write('{{' + var + '}}' + " likely doesn't exist\n")
            sys.exit()
    return True

def expand_containers(workflow):
    '''takes workflow and expands paths {{container.shifter.bbtools:"bryce911:bbtools/latest"}}'''
    _id = str(workflow['_id'])
    task_names = [task for task in workflow if not task.startswith("_")]
    for task in task_names:
        tmpcmd=list()
        container_found=False
        for cmd in workflow[task]['cmd'][-1]:
            variables = re.findall(r'{{(.*?)}}',cmd)
            for var in variables:
                if var.startswith(":shifter:"):
                    container_found = True
                    var_path = re.sub(r':shifter:',r'',var)
                    image = glom.glom(workflow,var_path)
                    container_string="shifter --image=" + image + " -- "
                    cmd = re.sub('{{' + var + '}}', container_string, cmd)
                elif var.startswith(":docker:"):
                    container_found = True
                    var_path = re.sub(r':docker:',r'',var)
                    image = glom.glom(workflow,var_path)
                    container_string="docker run --ulimit nofile=60000:60000 --user $(id -u):$(id -g) -v $PWD:/data -w /data " + image + " "
                    cmd = re.sub('{{' + var + '}}', container_string, cmd)
                elif var.startswith(":"):
                    sys.stderr.write("Only shifter and docker supported\n")
                    sys.exit(1)
            tmpcmd.append(cmd)
        if container_found:
            workflow[task]['cmd'].append(tmpcmd)
    return workflow

def get_dependencies(workflow,task_name):
  '''takes a command line, checks workflow and will generate a list of dependencide'''
  deps = []
  #set dependencies
  if 'depend' in workflow[task_name]:
      deps = workflow[task_name]['depend']
  cmd = "".join(workflow[task_name]['cmd'][-1])
  variables = re.findall(r'{{(.*?)}}',cmd)
  for var in variables:
    if var.startswith(task_name):
      continue
    if var.startswith("_"):
      continue
    if var.startswith(":"):
        continue
    #add dependencies
    deps=list(set(workflow[task_name]['depend'] + [var.split(".")[0]]))
  return deps
    

if __name__ == "__main__":
     main()


