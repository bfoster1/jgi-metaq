#!/usr/bin/env python

import sys,os
import json
import re
import time
from pymongo import MongoClient
import pymongo
from bson.objectid import ObjectId
import pika
import argparse
from time import sleep
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import utils

''' update client to be like metaq-add.py vars'''
#url = '54.212.246.70'
def main():
     '''main'''
     parser = argparse.ArgumentParser(description=main.__doc__)
     parser.add_argument("-s", "--server", required=False, default='172.31.17.117', help="server for mongodb and rabbitmq connections\n")
     parser.add_argument("-i", "--id", required=False, help="id\n")     
     args = parser.parse_args()

     #rabbitmq
     mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host=args.server,heartbeat=600,blocked_connection_timeout=300))
     channel = mq_connection.channel()
     channel.confirm_delivery()

     #mongodb
     client = MongoClient('mongodb://' + args.server + ':27017')
     db = client.workflows
     while 1:
          cursor = db.workflows
          #scan all tasks looking for most recent status of initialized
          for workflow in cursor.find():
               _id=str(workflow['_id'])
               if args.id and _id != args.id:
                    continue
               if '_status' not in workflow:
                    sys.stderr.write("Warning task with no status" + _id + "\n")
                    continue
#               elif workflow['_status'][-1]['status']=='Done':
#                    continue
#               elif workflow['_status'][-1]['status']=='Error':
#                    continue
#               elif workflow_has_error(workflow):
#                    continue
               elif workflow_all_complete(workflow)==True:
                    continue

               if "_config" in workflow and ":s3:infile" in workflow['_config']:
                    print(str(workflow['_id'])  + "\t" + workflow['_config'][":s3:infile"] + "\t" + workflow['_status'][-1]['status'])
               continue
               task_names = [task for task in workflow if not task.startswith("_")]
               for task_name in task_names:
                    if workflow[task_name]['_status'][-1]['status'] =='Initiated':
                         if is_launchable(workflow,task_name,db):
                              # get task dict and add _id
                              this_task = workflow[task_name]
                              this_task['_id'] = _id + ":" + task_name
                              if 'runner' not in this_task:
                                   sys.stderr.write("runner not in task "  + task_name + " " + _id + " marked Error\n")
                                   utils.update_status(db, str(_id), status='Error')
                              queue_name = this_task['runner']
                              # try to add to rabbitmq
                              try:
                                   channel.queue_declare(queue=queue_name, durable=True)
                              except:
                                   try:
                                        print("trying to reconnect");sys.stdout.flush()
                                        mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host=args.server,heartbeat=600,blocked_connection_timeout=300))
                                        channel = mq_connection.channel()
                                        channel.confirm_delivery()
                                        channel.queue_declare(queue=queue_name, durable=True)
                                   except:
                                        print("cant connect or reconnect moving on");sys.stdout.flush()
                                        continue

                              channel.basic_publish(exchange='',
                                                    routing_key=queue_name,
                                                    body=json.dumps(this_task),
                                                    properties=pika.BasicProperties(content_type='text/plain',delivery_mode=2),
                                                    mandatory=True)

                              utils.update_status(db, str(_id), taskname=task_name, status='Queued')
                              print(this_task['_id'] + " Queued");sys.stdout.flush()
          sys.exit()

          sleep(10)
     return

def update_status(db, workflow_id, task_name=None, status="", metadata={}):
    '''Takes db handle, workflow_id,taskname, status, and any other metadata and pushes status to db'''
    if isinstance(workflow_id,str):
        workflow_id=ObjectId(workflow_id)
    if metadata:
        status_update = {'status':status,'metadata':metadata, 'time':time.strftime("%Y-%m-%d %H:%M:%S")}
    else:
        status_update = {'status':status,'time':time.strftime("%Y-%m-%d %H:%M:%S")}
    if task_name is None:
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{'_status':status_update}})
    else:
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{task_name+'.status':status_update}})
    if not result:
        sys.stderr("Something wrong with status update\n")
        return False
    return True


def is_launchable(workflow,task_name,mongo):
     '''Takes a workflow and returns launchable task name list'''
     launchable = True
     this_task = workflow[task_name]
     if 'depend' in this_task and len(this_task['depend']) >0:
          for dep in this_task['depend']:
               if workflow[dep]['_status'][-1]['status']!='Done':
                    launchable=False
     return launchable

def workflow_has_error(workflow):
     '''takes a workflow and returns False if any task in err state'''
     task_names = [task for task in workflow if not task.startswith("_")]
     has_error=False
     for task_name in task_names:
          if workflow[task_name]['_status'][-1]['status']=="Error":
               has_error=True
     return has_error

def workflow_all_complete(workflow):
     '''takes a workflow and returns True if all tasks are "Done"'''
     task_names = [task for task in workflow if not task.startswith("_")]
     all_complete=True
     for task_name in task_names:
          if workflow[task_name]['_status'][-1]['status']!="Done":
               all_complete=False
     return all_complete
     
if __name__=='__main__':
     main()
