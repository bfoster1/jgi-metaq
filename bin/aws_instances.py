#!/usr/bin/env python

import subprocess
import boto3
import json 
import sys

url="54.212.246.70:8000"
url="172.31.17.117:8000"
def main():
    ec2 = boto3.resource('ec2')    
    instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
    for instance in instances:
        if not instance.tags:
            continue;
        tags = [tag['Value'] for tag in instance.tags]
        if 'uswest2-optimal-ce' in tags:
            ssh_cmd = "ssh -o \"StrictHostKeyChecking no\" -i ~/Downloads/bf-20190528-west2.pem ec2-user@" + instance.private_ip_address + " 2>/dev/null"
            find_cmd = "\"find /cromwell_root -mindepth 3 -maxdepth 3 -type d | xargs -i basename {} |  sort -u \""
            cmd = ssh_cmd + " " + find_cmd
            lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
            print(ssh_cmd)
            ids = [line.decode('utf-8').rstrip() for line in lines]
            for id_ in ids:
                status = get_status(id_)
                print(id_ + "\t" + status)

def get_status(cw_id):
    #curl -X GET "http://54.187.140.97:8000/api/workflows/v1/b756b8af-95ff-4833-81a1-05bc1bbaa397/status" -H  "accept: application/json"
    status = ""
    cmd = "curl -X GET \"http://" + url + "/api/workflows/v1/" + cw_id + "\" -H  \"accept: application/json\" 2>/dev/null "
    cmd = "curl -X GET \"http://" + url + "/api/workflows/v1/" + cw_id + "/metadata?expandSubWorkflows=false\" -H  \"accept: application/json\" 2>/dev/null "
    status = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()[0].decode('utf-8')
    j = json.loads(status)
    status = j['status']
    run_dir = ""
    inputs_json = json.loads(j['submittedFiles']['inputs'])
    if 'metadata' in inputs_json and "run_dir" in inputs_json['metadata'] :
        run_dir = inputs_json['metadata']['run_dir']
    elif "run_dir" in inputs_json:
        run_dir = inputs_json['run_dir']

    return status + "\t" + run_dir
                


if __name__ == '__main__':
    main()

