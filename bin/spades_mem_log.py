#!/usr/bin/env python

import os, sys
import json
import subprocess
import shutil
import re

if len(sys.argv)!=2 or not os.path.exists(sys.argv[1]):
    sys.exit("Usage: " + sys.argv[0] + " file\n")


def main():
    mem=list()
    with open(sys.argv[1], "r") as infile:
        time = list()
        for line in infile.readlines():
            line = line.rstrip()
            if re.match(r'^\s+\d',line):
                if re.match(r'^.*?\s\d+G\s\/\s\d+G.*',line):# and not re.match(r'^.*kmer_splitters.*$',line):
                    mem.append(re.sub(r'^.*?\s(\d+)G\s\/\s\d+G.*$', r'\1',line.rstrip()))                    
#                if re.match(r'^.*\d+G\s\/\s\d+G\s',line):                 #43G / 151G
#                    mem.append(re.sub(r'^.*(\d+)G\s\/\s\d+G\s', r'\1',line.rstrip()))

            if line.find('ssembling time') != -1:
#                import pdb;pdb.set_trace()
                #Assembling time: 0 hours 0 minutes 46 seconds
                h = re.sub('^.*\s(\d+)\shours.*$',r'\1',line)
                m = re.sub('^.*\s(\d+)\sminutes.*$',r'\1',line)
                time.append(float(m)/60 + float(h))
#    print sorted(mem)
    print("#path\tmaxmem\tkmer_runtime\ttotal_runtime")
    print(sys.argv[1] + "\t" + str(sorted([int(i) for i in mem])[-1]) + "\t" + ",".join([str(round(i,2)) for  i in time]) + "\t" + str(round(sum(time),2)) )
    return
                 

if __name__ == "__main__":
    main()

#lines = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()

#line = re.sub(r'^.*\/(.*).*$', r'\1' , line)

#with open(sys.argv[1], "r") as infile:
#    json_file=json.loads(infile.read())

#with open(sys.argv[1] + ".mod", "w") as outfile:
#    outfile.write(json.dumps(json_file,indent=3))

#shutil.move(metadata, metadata + ".bak")

