#!/usr/bin/env python
'''
jgi specific reporting utils
Created on Jan 24, 2019

@author: bfoster
'''
import os, sys
import urllib.request
import collections
import json
import time 
import textwrap
import glob 
import subprocess
import re
from bson.objectid import ObjectId
import pymongo 
RQC_REPORT = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"
def main():

    
    #filename="/global/dna/dm_archive/rqc/analyses/AUTO-137286/12074.1.235970.AGCTAAC-GGTTAGC.filter-METAGENOME.fastq.gz"
    #su = filename_to_sequnit(filename)
    #Tests used in development of functions
    #print(its_ap_to_taxonoid(1064167))
    #exit()
    #with open("test.json", "w") as f:f.write(json.dumps(web_services_test(),indent=3));exit()
    #print(json.dumps(web_services_test(),indent=3));exit()
    #print(filename_to_sequnit(fn))
    #print(json.dumps(sequnit_to_lib_spid(su, debug=True),indent=3))
    #print(sequnit_to_rqc_status('11789.1.219619.GTTCGGT-AACCGAA.fastq.gz',debug=False))#usable false
    #print(sequnit_to_rqc_status('12074.1.235970.AGCTAAC-GGTTAGC.fastq.gz',debug=False))#usable true

    #test get latest su
    #filtered = list()
    #filtered += ["/global/dna/dm_archive/rqc/analyses/AUTO-82375/11789.1.219619.GTTCGGT-AACCGAA.filter-METAGENOME.fastq.gz", "/global/dna/dm_archive/rqc/analyses/AUTO-137286/12074.1.235970.AGCTAAC-GGTTAGC.filter-METAGENOME.fastq.gz"]
    #filtered += ["/global/dna/dm_archive/rqc/analyses/AUTO-137286/12074.1.235970.AGCTAAC-GGTTAGC.filter-METAGENOME.fastq.gz"]
    
    #su = [filename_to_sequnit(filename) for filename in filtered]actual_library_creation_queue:
    #print(json.dumps(sequnit_to_pi_info(su[0]),indent=3))
    #atid = task[0]['at'][0]['atid']
    #print(json.dumps(at_to_pipeline_templates(atid,files=filtered),indent=3))
    #print(rqc_pipeline_queue_type_by_ap_at("84_47"))
    return


'''functions'''

def update_status(db, workflow_id, taskname=None, status="", metadata={}):
    '''Takes db handle, workflow_id,taskname, status, and any other metadata and pushes status to db'''
    if isinstance(workflow_id,str):
        workflow_id=ObjectId(workflow_id)

    if metadata:
        status_update = {'status':status,'metadata':metadata, 'time':time.strftime("%Y-%m-%d %H:%M:%S")}
    else:
        status_update = {'status':status,'time':time.strftime("%Y-%m-%d %H:%M:%S")}

    if taskname is None:  #update workflow status
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{'_status':status_update}})
    else: #update task status
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{taskname+'._status':status_update}})

    if not result: 
        sys.stderr.write("Something wrong with status update:" + workflow_id + " " + taskname + " " + status_update + "\n")
        return False
    return True



def update_status_older(db, workflow_id, taskname=None, status="", metadata={}):
    '''!!!!!!!!!!!!!!!!!!!!!!!Fix latetr !!!!!!!!!!!!!!!!!!!!!!'''
    '''Takes db handle, workflow_id,taskname, status, and any other metadata and pushes status to db'''
    print(type(db))
    if isinstance(workflow_id,pymongo.database.Database):
        workflow_id=ObjectId(workflow_id)
    if metadata:
        status_update = {'status':status,'metadata':metadata, 'time':time.strftime("%Y-%m-%d %H:%M:%S")}
    else:
        status_update = {'status':status,'time':time.strftime("%Y-%m-%d %H:%M:%S")}
    if taskname is None:
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{'_status':status_update}})
    else:
        result = db.workflows.find_one_and_update({"_id":workflow_id}, {'$push':{task_name+'.status':status_update}})
    if not result:
        sys.stderr.write("Something wrong with status update" + json.dumps(result,indent=3))
        return False
    return True

def get_sigs_table(meta, kvp, readstats_path, covstats_path):
    '''
    Given metadata, and key value pairs, and paths containing bbreadstats and covstats create sigs table for rqc stats.
    args: metadata data structure
    returns: kvp with "assy.SIGS" table added"

    Table 3. Library information.
    Sample Label(s)
    Sample prep method
    Library prep method(s)
    Sequencing platform(s)
    Sequencing chemistry
    Sequence size (GBp)
    Number of readslogical_amount_units:
    Single-read or paired-end sequencing
    Sequencing library insert size
    Average read length
    Standard deviation for read length

    Table 4. Sequence processing
    Tool(s) used for quality control
    Number of sequencs removed by quality control procedures
    Number of sequences that passed quality control procedures
    Number of artificial duplicate reads

    Table 5. Metagenome statistics
    Libraries Used
    Assembly tool(s) used
    Number of contigs after assembly
    Number of singletons after assembly
    minimal contig length
    Total bases assembled
    Contig n50
    % of Sequences assemble 
    Measure for % assembled (either from assembly output or via mapping of reads to contigs; in the latter case provide tool and parameters used)
    '''
    def retrieve_seq_stats(info_list,key,omit_empty_val=False,sorted_val=False,unique=False):
        '''Retrieves seq stat list from metadata section seq_info for a particular key'''
        retrieved_list = list()
        for info in info_list:
            if key in info['info'] and bool(info['info'][key]):
                retrieved_list.append(info['info'][key])
            elif omit_empty_val==False:
                retrieved_list.append(None)
        if unique:
            retrieved_list=list(set(retrieved_list))
        if sorted_val:
            retrieved_list=sorted(retrieved_list)
        return retrieved_list
    if not(readstats_path) or not(os.path.exists(readstats_path)):
        sys.stderr.write("cant find read stats")
        sys.exit(255)

    if not(covstats_path) or not(os.path.exists(covstats_path)):
        sys.stderr.write("cant find coverage stats")
        sys.exit(255)
    
    #SIG table 3
    readlen=0
    stdev=0
    with open (readstats_path, "r") as f:
        for line in f.readlines():
            if line.startswith("#Avg:"):
                readlen = float(re.sub(r'^#Avg:\s+(.*)$',r'\1',line.rstrip()))
            if line.startswith("#Std_Dev:"):
                stdev = re.sub(r'^#Std_Dev:\s+(.*)$',r'\1',line.rstrip())

    kvp["assy.SIG.3.Average_read_length"] = "{:.1f}".format(readlen)
    kvp["assy.SIG.3.Standard_deviation_for_read_length"] = stdev
    
    if 'seq_info' in meta['metadata']:
        seq_info = meta['metadata']['seq_info']
        #base_count = str(sum([info['info']['raw_base_count'] for info in meta['seq_info']]))
        base_count = str(sum(retrieve_seq_stats(seq_info, 'raw_base_count')))
        kvp["assy.SIG.3.Sequence_size_GBp"] = round(float(base_count)/1000000000,1)
        kvp["assy.SIG.3.Sequence_size_GBp"] = "{:.1f}".format(kvp["assy.SIG.3.Sequence_size_GBp"])
        kvp["assy.SIG.3.Number_of_reads"] = str(sum(retrieve_seq_stats(seq_info,'raw_count')))
        kvp["assy.SIG.3.Sample_prep_method"]=""
        kvp["assy.SIG.3.Sample_Labels"] = ",".join(retrieve_seq_stats(seq_info,'sample_labels',omit_empty_val=True,unique=True,sorted_val=True))
        kvp["assy.SIG.3.Library_prep_methods"] = ",".join(retrieve_seq_stats(seq_info,'sample_methods',omit_empty_val=True,unique=True,sorted_val=True))
        kvp["assy.SIG.3.Sequencing_platforms"] = ",".join(retrieve_seq_stats(seq_info,'platform_name',omit_empty_val=True,unique=True,sorted_val=True))
        kvp["assy.SIG.3.Single-read_or_paired-end_sequencing"] = ",".join(retrieve_seq_stats(seq_info,'run_type',omit_empty_val=True,unique=True,sorted_val=True))

        if kvp["assy.SIG.3.Sequencing_platforms"].find("llumina") != -1:
            kvp["assy.SIG.3.Sequencing_chemistry"] = "SBS"
        else:
            kvp["assy.SIG.3.Sequencing_chemistry"]=""
        from statistics import mean 
        kvp["assy.SIG.3.Sequencing_library_insert_size"] = str(mean(retrieve_seq_stats(seq_info,'insert_size')))
    
        #SIG table 4
        kvp["assy.SIG.4.Tools_used_for_quality_control"] = 'pipeline version(s) ' + ",".join(retrieve_seq_stats(seq_info,'filter_pipeline_version',omit_empty_val=True,unique=True,sorted_val=True))
        kvp["assy.SIG.4.Tools_used_for_quality_control"] += ' from bbtools version ' + ",".join(retrieve_seq_stats(seq_info,'bbtools_version',omit_empty_val=True,unique=True,sorted_val=True))
        raw_count = sum(retrieve_seq_stats(seq_info,'raw_count'))
        filtered_count = sum(retrieve_seq_stats(seq_info,'filtered_count'))
        kvp["assy.SIG.4.Number_of_sequences_that_passed_quality_control_procedures"] = filtered_count
        kvp["assy.SIG.4.Number_of_sequences_removed_by_quality_control_procedures"] = raw_count - filtered_count
        kvp["assy.SIG.4.Number_of_artificial_duplicate_reads"] = "NA"

        #SIG table 5
        kvp["assy.SIG.5.Libraries_Used"] = ",".join(sorted(list(set(meta['metadata']['library_name']))))
        assembly_prefix = "";alignment_prefix = ""
        if "outputs.metatranscriptome_assembly.assembler" in kvp: #metatranscriptome
            assembly_prefix = "outputs.metatranscriptome_assembly."
            alignment_prefix = "outputs.metatranscriptome_alignment."
        elif  "outputs.contigs.assembler" in kvp:#metagenome
            assembly_prefix = "outputs.contigs."
            alignment_prefix = "outputs.metagenome_alignment."
        else:
            sys.stderr.write("Can't find assembley, alignment record")
            exit()
        
        kvp["assy.SIG.5.Assembly_tools_used"] = kvp[assembly_prefix + "assembler"] + "_" + kvp[assembly_prefix + "assembler_version"]
        kvp["assy.SIG.5.Number_of_contigs_after_assembly"] = int(kvp[assembly_prefix + "n_contigs"])
        kvp["assy.SIG.5.Total_bases_assembled"] = int(kvp[assembly_prefix + "contig_bp"])
        kvp["assy.SIG.5.Contig_n50"] = int(kvp[assembly_prefix + "ctg_L50"])
        kvp["assy.SIG.5.Measure_for_percent_assembled"] = "\"reads used as input to the assembler were mapped back to the assembled reference using aligner " 
        kvp["assy.SIG.5.Measure_for_percent_assembled"] += kvp[alignment_prefix + "aligner"] + " version " + kvp[alignment_prefix + "aligner_version"] 
        kvp["assy.SIG.5.minimal_contig_length"] = 200

        #total_reads_input_to_aligner = all_filtered_count; # kvp["assy.SIG.4.Number_of_sequences_that_passed_quality_control_procedures"]
        all_filtered_count = sum(retrieve_seq_stats(seq_info,'filtered_count'))
        sum_aligned = 0
        sum_aligned_gt10k = 0
        with open(covstats_path, "r") as readfile:
            for line in readfile:
                if line.startswith("#"):
                    continue
                fields = line.split("\t")
                sum_aligned += int(fields[6]) + int(fields[7])
                if int(fields[2]) > 10000:
                    sum_aligned_gt10k += int(fields[6]) + int(fields[7])
        perc_aligned = "{:.1f}".format(float(sum_aligned/float(all_filtered_count)*100))
        perc_gt10k_aligned = "{:.1f}".format(float(sum_aligned_gt10k/float(all_filtered_count)*100))
        kvp["assy.SIG.5.percent_of_Sequences_assembled"] = perc_aligned
        kvp["assy.SIG.5.percent_of_Sequences_assembled_gt10k"] = perc_gt10k_aligned
        kvp["assy.SIG.5.Number_of_singletons_after_assembly"] =  all_filtered_count - sum_aligned
    return kvp
          
def create_sig_subset(kvp):
    '''Takes the kvp file and separated out the SIGS tables'''
    return_dict = hasher()
    for k in kvp:
        if k.find("SIG") != -1:
            sig_string = re.sub(r'^.*SIG\.(\d).*$',r'\1',k.rstrip())
            if sig_string not in return_dict:
                return_dict[sig_string]['data'] = list()
            return_dict[sig_string]['data'].append(k + "=" + str(kvp[k]))
    return_dict['3']['filename'] = 'Table_3_library_information.txt'
    return_dict['4']['filename'] = 'Table_4_sequence_processing.txt'
    return_dict['5']['filename'] = 'Table_5_metagenome_statistics.txt'
    return return_dict

def get_logical_amounts(info):
    '''Given a list of seq_info meta data, return dict of { locigal_amount_total:100, logical_amount_units:Gb, combined_assembly:False}'''
    return_dict = dict()
    return_dict['is_combined_assembly'] = 1 if len(info)>1 else 0
    units = list(set([k['info']['logical_amount_units'] for k in info]))
    if len(units)!=1:
        sys.exit("Mixed logical units")
    elif units[0] =='Gb':
        return_dict['logical_amount_total'] =   round(sum([k['info']['raw_base_count'] for k in info])/1000000000,2)
        return_dict['logical_amount_units'] = 'Gb'
    elif units[0] =='M reads':
        return_dict['logigal_amount_total'] =   round(sum([k['info']['raw_count'] for k in info])/1000000,2)
        return_dict['logical_amount_units'] = 'M reads'
    else:
        print(units[0]);exit()
    return return_dict


#external report calls

'''external report calls'''

def generate_rqc_report(sequnits=[], run_type="metag", outputfile="", rqcstats="",  debug=False):
    '''Takes metadata dict, directory name, filename, rqc_kvp and runs rqc_report'''
    #module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py  -t mt-summary.tex -i BZWCA -of rqc-stats.pdf -f ../../rqc-stats.txt -o $PWD ... works
    #module load jgi-rqc;$JGI_RQC_DIR/report/rqc_report.py  -t mg-summary.tex -i BZWCA -f ../rqc-stats.txt,../rqc-files.txt -of rqc-stats.pdf
    cmd = 'source $(dirname $(which python))/deactivate 2>/dev/null; conda deactivate 2>/dev/null ;source /projectb/sandbox/gaag/bfoster/miniconda2/etc/profile.d/conda.sh; conda activate; ' 
    if run_type=="metat":
        cmd += RQC_REPORT + " -t mt-summary.tex "
    elif run_type=="metag":
        cmd += RQC_REPORT + " -t mg-summary.tex "
    else:
        exit("run_type must be: [metag,metat]")
    if not(all([os.path.exists(f) for f in rqcstats.split(',')])):
        exit(rqcstats + " does not exist")
    cmd += " -i " + ",".join(sequnits)
    cmd += " -of " + os.path.basename(outputfile)
    cmd += " -f " + rqcstats
    cmd += " -o " + os.path.dirname(outputfile)
    if debug:
        print(cmd)
        print("starting")
    
    out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()
    if not(os.path.exists(outputfile)):
        sys.exit(cmd + " not successful\n" + str(out))
    print("fininshed")
    return outputfile  

def generate_graphics(coverage_file, out_dir, prefix="report"):
    '''Graphics routine from LANL
    input: coverage file, outputdir for graphics, and filename prefix
    ouputs: report_avg_fold_vs_len.bmp report_gc.bmp report_gc_vs_avg_fold.bmp'''
    
    #graphics routine from LANL
    rscript = os.path.join(out_dir,prefix + ".R")
    outstub =  os.path.join(out_dir,prefix)
    outcov = outstub + '_avg_fold_vs_len.bmp'
    outgccov = outstub + '_gc_vs_avg_fold.bmp'
    outgc = outstub + '_gc.bmp'
    with open(rscript,"w") as f:
        f.write('a<-read.table(file="' + coverage_file + '",header=TRUE,comment="",sep="")' + "\n")
        f.write('bitmap("' + outcov + '")' + "\n")
        f.write('plot(a$Length,a$Avg_fold,pch=8,xlab="Contigs Length (bp)",ylab="Average coverage fold (x)",cex=0.7, main="Contigs average fold coverage vs. Contigs Length",log="y")' + "\n")
        f.write('tmp<-dev.off();')
        f.write('bitmap("' + outgccov + '");' + "\n")
        f.write('par(mar = c(5, 5, 5, 5), xpd=TRUE, cex.main=1.5, cex.lab=1.2);' + "\n")
        f.write('plot(a$Avg_fold,a$Ref_GC,ylab="GC (%)",xlab="Average coverage fold (x)", main="Contigs average fold coverage vs. GC",pch=20,col="blue",cex=log(a$Length/mean(a$Length)),log="x");tmp<-dev.off();' + "\n" )
        f.write('bitmap("' + outgc + '");hist(subset(a,a$Length>0)$Ref_GC*100,seq(from=0,to=102,by=1.5),main="GC Histogram for contigs",ylab="# of contigs",xlab="GC (%)");dev.off();quit();' + "\n")
    cmd = ' cat ' + rscript + ' | R --vanilla --slave --silent 1>| ' + rscript + '.o' + ' 2>| ' + rscript + '.e'
    out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    if 0:print(out)
    return (outcov, outgccov, outgc)

def generate_pdf(**kwargs):
    '''takes txt, graphics, outdir, prefix args and returns (txtout, htmlout, and pdfout) '''
#   kwargs['txt']
#   kwargs['graphics'] list
#   kwargs['outdir']
#   kwargs['prefix']

    txtout =  os.path.join(kwargs['outdir'],kwargs['prefix'] +'.txt')
    htmlout = os.path.join(kwargs['outdir'],kwargs['prefix'] +'.html')
    pdfout = os.path.join(kwargs['outdir'],kwargs['prefix'] +'.pdf')
    with open(txtout,"w") as f:
        f.write(kwargs['txt'] + "\n")

    with open(htmlout,"w") as f:
        f.write('''
    <html>
        <head>
        </head>
        <body bgcolor="#ffffff" text="#000000" link="#0000ff" vlink="#ff0000" alink="#ffff00">
        <pre>
        ''')
        f.write(kwargs['txt'] + "\n")
        f.write("        </pre>\n")
        for image in [os.path.abspath(filename) for filename in  kwargs['graphics']]:
            f.write('<img src="' + image + '"/>'  + "\n")
        f.write("            </body>\n")
        f.write("</html>\n")
    cmd =  'env -i /bin/bash -c "source /projectb/sandbox/gaag/bfoster/miniconda3/etc/profile.d/conda.sh; conda activate; wkhtmltopdf ' + htmlout + ' ' + pdfout + '"' 
    if 'debug' in kwargs and kwargs['debug']==True:
        print(cmd)
    out=""
    try:
#        out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
    except:
        print("Failed:" + cmd)
        print(out)
        sys.exit()
    return (txtout,htmlout, pdfout)

'''AP/AT'''

def atid_to_apid(atid):
    '''Given atid, return apid'''
    ap = web_services('pmo','at', int(atid))['uss_rw_analysis_task']['analysis_project']['analysis_project_id']
    return int(ap)
    
def task_getter(atid=None,apid=None,exclude_closed=True,force_latest=False, task_type=None):
    '''Given input apid or apid return task info'''
    task_types = {'DNA Assembly':47, 'RNA Assembly and Mapping':83, 'Mapping to Reference':44, 'Co-assembly': 25}
    #starts with apid
    if atid is not None and apid is not None:
        sys.exit("Can't specify atid and apid for get_mapping_at")
    elif atid is not None:
        apid = int(atid_to_apid(atid))
    elif apid is not None:
        apid = int(apid)
    else:
        sys.exit("Specify atid or apid for get_mapping_at")

    #get task_type id if required 0 is None
    if task_type in task_types.values():
        task_type_id = int(task_type)
    elif task_type in task_types.keys():
        task_type_id = int(task_types[task_type])
    elif task_type is not None:
        sys.exit("Task type:" + str(task_type) + " not supported")
    else:
        task_type_id = 0    
        
    #get all tasks per ap
    task_list = list()

    ap = web_services('pmo','ap', apid)['uss_analysis_project']
    ap_keep = ["analysis_product_id","analysis_product_type_name","analysis_project_id","analysis_project_name"]
    ap_record = {key:ap[key] for key in ap if key in ap_keep}
    
    fdid = ap['final_deliv_project_id']
    fd = web_services('pmo','fd', fdid)['final_deliverable_project']
    fd_keep = ['final_deliv_project_id', 'final_deliv_project_name', 'final_deliv_product_id']
    fd_record = {key:fd[key] for key in fd if key in fd_keep}


    ats = ap['analysis_tasks']
    at_keep = ["analysis_task_id","analysis_task_type_id","analysis_task_type_name","current_status_id","input_analysis_task_ids","status_date","status_name"]

    for task in ats:
        tmp = dict()
        tmp['at']={key:task[key] for key in task if key in at_keep}
        tmp['fd']=fd_record
        tmp['ap']=ap_record
        
        spid_list = [i['sequencing_project_id'] for i in ap['sequencing_projects']]
        tmp['spid']={key:dict() for key in spid_list}
        
        #getting the source information for mapping to reference projecs 
        source_ap = None
        actual_source_ap = None
        if 'img_dataset_id' in ap and ap['img_dataset_id'] is not None: #taxonoid to APid
            source_ap = ap['img_dataset_id']
            record = web_services('gold', 'taxonoid', source_ap)
            actual_source_ap = record['itsAnalysisProjectId']
        elif 'source_analysis_project_id' in ap and ap['source_analysis_project_id'] is not None:
            source_ap = ap['source_analysis_project_id']
            actual_source_ap = source_ap
        tmp['source_ap'] = source_ap
        tmp['actual_source_ap'] = actual_source_ap
        task_list.append(tmp)
        
    #filter by atid if requested
    if atid is not None:
        task_found = False
        tmp=list()
        for task in task_list:
            if int(task['at']['analysis_task_id'])==int(atid):
                tmp.append(task)
                task_found = True
        if not task_found:
            sys.exit("atid:" + str(atid) + " not found in apid:" + str(apid))
        else:
            task_list = tmp
    
    #filter by task_type_id if requested
    if (task_type_id != 0):
        task_found = False
        tmp=list()
        for task in task_list:
            if int(task['at']['analysis_task_type_id'])==int(task_type_id):
                tmp.append(task)
                task_found = True
        if not task_found:
            sys.exit("analysis_task_type_id:" + str(task_type_id) + " not found in apid:" + str(apid))
        else:
            task_list = tmp
        
    #filter by status if requested
    if (exclude_closed):
        closed_status=[9, 10, 12, 13, 99]
        task_found = False
        tmp=list()
        for task in task_list:
            if int(task['at']['current_status_id']) not in closed_status:
                tmp.append(task)
                task_found = True
        if not task_found:
            sys.exit("analysis_task_type_id:" + str(task_type_id) + " not found in apid:" + str(apid))
        else:
            task_list = tmp

    #grab latest if requested    
    if len(task_list)>1 and force_latest==True:
        task_list.sort(key=lambda e: e['at']['status_date'],reverse=True)
        task_list = [task_list[0]]

        
    return task_list

def ap_to_assembly_contigs(apid):
    '''Takes apid and returns [{file_path:path, file_name:name, full_path:fullpath, jamo_status:RESTORED, added_date:date }]'''
    ref = web_services('jamo', 'json', {"metadata.analysis_project_id":int(apid),"file_type":"contigs"})
    if len(ref)>1:
        sys.exit("More than one contigs file")

    field_keep = ['_id','file_path','file_name', 'file_status','file_status_id', 'added_date','seq_unit_name']
    record = {key:ref[0][key] for key in ref[0] if key in field_keep}
    record['analysis_project_id']=apid
    record['full_path']=os.path.join(record['file_path'],record['file_name'])
    return record

'''JAMO/files'''    

def get_latest_filtering(filename):
    '''takes a filtered file and returns the full path of the filtering most recently added to jamo'''
    filename = os.path.basename(filename)
    jamo_filtered = web_services('jamo', 'json', {'file_name':os.path.basename(filename),"metadata.fastq_type":"filtered"})
    jamo_filtered.sort(key=lambda e: e['added_date'],reverse=True)
    full_path = os.path.join(jamo_filtered[0]['file_path'],jamo_filtered[0]['file_name'])
    return full_path 


def filtered_file_to_report_text(filename,force_latest=False):
    '''Takes a filtered file and returns all lib info
    input = input file
    output = text of library info for reports
    '''
    return_dict = hasher()
    info = ""
    filename = os.path.basename(filename)
    sequnit = filename_to_sequnit(filename)
    rqc_dump = web_services('rqc', 'su', sequnit)
    unique_31_mer = 10000000000#default
    if 'stats' in rqc_dump and 'filter:contamtrimunique31mers_num' in rqc_dump['stats']:
        unique_31_mer = rqc_dump['stats']['filter:contamtrimunique31mers_num']

    jamo_sequnit = web_services('jamo', 'json', {'file_name':os.path.basename(sequnit)})
    if len(jamo_sequnit)>1:
        sys.stderr.write("Multiple sequnit files found")
    else:
        jamo_sequnit = jamo_sequnit[0]['metadata']
    
    jamo_filtered = web_services('jamo', 'json', {'file_name':os.path.basename(filename),"metadata.fastq_type":"filtered"})
    if len(jamo_filtered)!=1 and force_latest:
        jamo_filtered.sort(key=lambda e: e['added_date'],reverse=True)
    elif len(jamo_filtered)!=1:
        sys.stderr.write("Multiple filtered files found")
        sys.exit()
        
    jamo_file_status_id = jamo_filtered[0]['file_status_id']
    jamo_filtered = jamo_filtered[0]['metadata']
    
    #initialize
    stats_keys = ['instrument_type', 'platform_name','library_protocol','target_fragment_size_bp','raw_count','raw_base_count', 'filtered_count', 'filtered_base_count']
    lib = {k:"" for k in stats_keys}
    if 'physical_run' in jamo_sequnit and 'instrument_type' in jamo_sequnit['physical_run']:
        lib['instrument_type']=jamo_sequnit['physical_run']['instrument_type']
    if 'physical_run' in jamo_sequnit and 'platform_name' in jamo_sequnit['physical_run']:
        lib['platform_name'] = jamo_sequnit['physical_run']['platform_name']
    if 'sow_segment' in jamo_sequnit:
        if 'library_protocol' in jamo_sequnit['sow_segment']:
            lib['library_protocol'] = jamo_sequnit['sow_segment']['library_protocol']
        if 'target_fragment_size_bp' in jamo_sequnit['sow_segment']:
            lib['target_fragment_size_bp'] = jamo_sequnit['sow_segment']['target_fragment_size_bp']
    lib['raw_sequnit_name']=sequnit
    lib['unique31mers']=unique_31_mer
    lib['raw_count'] = int(jamo_filtered.get('input_read_count', ''))
    lib['raw_base_count'] = int(jamo_filtered.get('input_base_count', ''))
    lib['filtered_filename']=filename
    lib['filtered_count'] = int(jamo_filtered.get('filter_reads_count', '')) 
    lib['filtered_base_count'] = int(jamo_filtered.get('filter_base_count', '')) 
    lib['filtered_file_status'] = int(jamo_file_status_id)
    lib['library_name']=jamo_sequnit['library_name']
    lib['filter_pipeline_version']=jamo_filtered.get('filter_pipeline_version', '')
    lib['bbtools_version']=jamo_filtered.get('bbtools_version', '')
    lib['sample_labels']=jamo_filtered['sow_segment'].get('sample_name', '')
    lib['sample_methods']=jamo_filtered['sow_segment'].get("actual_library_creation_queue", '')
    lib['run_type']=jamo_filtered['sow_segment'].get("physical_run_type", "Paired") #single or paired
    lib['logical_amount_units']=jamo_filtered['sow_segment'].get('logical_amount_units', "")
    try:
        lib['insert_size']=float(jamo_sequnit['rqc']['read_qc']['illumina_read_insert_size_avg_insert'])
        lib['insert_size_std']=float(jamo_sequnit['rqc']['read_qc']['illumina_read_insert_size_std_insert'])
        lib['read1_len']=float(jamo_sequnit['rqc']['read_qc']['read1_length'])
        lib['read2_len']=float(jamo_sequnit['rqc']['read_qc']['read2_length'])
    except:
        lib['insert_size']="NA"
        lib['insert_size_std']="NA"
        lib['read1_len']="NA"
        lib['read2_len']="NA"
        
    info += "Library: " + lib['library_name'] + "\n"
    info += "Platform: " + lib['platform_name'] + " " + lib['instrument_type'] + " " + lib['library_protocol'] +  " " + str(lib["target_fragment_size_bp"]) +  " bp fragment\n" ;
    info += "Filtered Data: " + lib['filtered_filename'] + "\n"
    info += "Filtered Read Count: " + str(lib["filtered_count"]) + "\n"
    info += "Filtered Base Count: " + str(lib["filtered_base_count"]) + "\n"
    info += "Raw Data: " + lib['raw_sequnit_name'] +  "\n"
    info += "Raw Read Count: " + str(lib["raw_count"]) + "\n"
    info += "Raw Base Count: " + str(lib["raw_base_count"]) + "\n"
    return_dict['readme_text'] = info
    return_dict['info'] = lib
    return return_dict

def jamo_release_count(_id,type_id="spid"):
    '''takes id and returns jamo result count'''
    if type_id not in ['spid','atid']:
        exit("must specify atid spid")
    if type_id=='spid':
        result = web_services('jamo', 'json', {"metadata.sequencing_project_id":int(_id), "file_type":"scaffolds"})
    else:
        result = web_services('jamo', 'json', {"metadata.analysis_task_id":int(_id), "file_type":"scaffolds"})
    if result !=0:  
        return str(len(result))
    else:
        return ""

def filename_to_sequnit(filename, debug=False):
    '''Takes a jgi file if type(sequnits)==type(str()):
        sequnits = [sequnits]ename or path (ex. rqc filtered) and returns the sequnit name'''
    sequnit = ""
    filename = os.path.basename(filename)
    jamo_records = web_services("jamo","json",{"file_name":filename}, debug=debug)
    parent_sequnit_ids = list()
    for record in jamo_records:
        parent_sequnit_ids=list(set(record['metadata'].get("seq_unit_name",["NA"]) + parent_sequnit_ids))
    if debug:
        print(parent_sequnit_ids)
    if len(parent_sequnit_ids)==0:
        sys.stderr.write("Sequnit not found for: " + filename + "\n")
    elif len(parent_sequnit_ids)==1:
        sequnit = parent_sequnit_ids[0]        
    elif len(parent_sequnit_ids)>1:
        sys.stderr.write("Multiple sequnits: " + str(parent_sequnit_ids) + " found for: " + filename + "\n")    
        sys.exit()
    else:
        sys.stderr.write("Something wrong")
        sys.exit()
    return sequnit

def spid_to_filtered_info(spid, force_latest=False, allow_unusable=False):
        '''takes spid and fills returns list with [{file_path:path, jamo_status:RESTORED, added_date:date, rqc_status:true }] Has to be jat submitted filtered'''
        files = web_services("jamo","json",{"metadata.sequencing_project_id":int(spid),"metadata.fastq_type":"filtered"})
        return_list = list()
        filt_keep = ['_id','file_path','file_name', 'file_status','file_status_id', 'added_date','seq_unit_name']
        for filt in files:
            tmp = dict()
            tmp = {key:filt[key] for key in filt if key in filt_keep}
            if 'metadata' not in filt or 'seq_unit_name' not in filt['metadata']:  #THIS THROWS out old non jat filtered.
                continue
            tmp['seq_unit_name']=filt['metadata']['seq_unit_name'][0]
            tmp['library_name']=filt['metadata']['library_name']
            tmp['usable'] = sequnit_to_rqc_status(filt['metadata']['seq_unit_name'][0])
            tmp['full_path'] = os.path.join(tmp['file_path'],tmp['file_name'])
            tmp['library_info']=filtered_file_to_report_text(tmp['full_path'],force_latest=force_latest)
            tmp['file_status_id']=filt['file_status_id']
            if allow_unusable and tmp['usable']==0:
                return_list.append(tmp)
            elif tmp['usable'] !=0:
                return_list.append(tmp)
        return return_list
    
'''RQC queries/info'''
    
def rqc_pipeline_queue_and_templates_by_ap_at(ap_at,file_names=[],config_file="",debug=False):
    '''Takes: "ap_at" and a list of files
    unique31 unique kmer count is retrieved, 
    logic based on multi/seq, unique kmer and product is used to return pipeline queue
    Returns: a dict with 
    {"rqc_q_id":13, "rqc_q_name":"MG on 250gb nodes",
        "templates":{
            "analysis":"analysis.metag.spades.2.0.yaml",
            "report":"reporting.metag.spades.1.0.yaml",
            "desc":"spades w/bfc for cori/denovo"
        }
    }'''
    config_text=""
    if (config_file):
        with open(config_file,"r") as f:config_text=f.read()
    else:
        config_text='''{
        "ap_at":{
            "58_47":{"name":"Metagenome Annotated Minimal Draft","analysis_template":2},
            "7_47":{"name":"Metagenome Annotated Standard Draft","analysis_template":2},
            "84_47":{"name":"Metagenome Viral Annotated Standard Draft","analysis_template":2},
            "10_83":{"name":"Metagenome Annotated Metatranscriptome","analysis_template":3},
            "38_83":{"name":"Metagenome Annotated Metatranscriptome (combined)","analysis_template":3},
            "68_83":{"name":"Co-Assembly - Metagenome Metatranscriptome","analysis_template":3},
            "61_83":{"name":"Eukaryote Community Metatranscriptome","analysis_template":3},
            "69_83":{"name":"Co-Assembly - Eukaryotic Community Metatranscriptome","analysis_template":3},
            "101_47":{"name":"Metagenome Viral Annotated Minimal Draft","analysis_template":4},
            "53_47":{"name":"Cell Enrichment unamplified Random","analysis_template":4},
            "55_47":{"name":"Cell Enrichment unamplified Targeted","analysis_template":4},
            "40_25":{"name":"Combined Assembly - co-assembly","analysis_template":2}
        },
        "rqc_queues":{
            "13":"Metagenome",
            "41":"MG on 250gb nodes",
            "42":"MG on 500gb nodes",
            "43":"MG on 1000gb nodes",
            "18":"Metatranscriptome Assembly and Alignment",
            "44":"MG-Viral"
        },
        "analysis_templates":{
            "1":{"id":1,"analysis":"analysis_metag_bfc_spades.yaml",  "report":"report_metag_bfc_spades.yaml","desc":"spades w/bfc for cori/denovo"},
            "2":{"id":2,"analysis":"analysis_metag_bbcms_spades.yaml","report":"report_metag_bbcms_spades.yaml","desc":"spades w/bbcms for cori/denovo"},
            "3":{"id":3,"analysis":"analysis_metat_megahit.yaml",     "report":"report_metat_megahit.yaml","desc":"megahit for cori/denovo"},
            "4":{"id":4,"analysis":"analysis_metag-lowv_spades.yaml", "report":"report_metag-lowv_spades.yaml","desc":"spades --sc w/tadpole/dedupe for cori denovo"}
        }
        }'''
    template_info=None
    rqc_q_id = None
    rqc_q_name=None
    ap_at_mapping=json.loads(config_text)
    if ap_at in ap_at_mapping['ap_at']:
        template = ap_at_mapping['ap_at'][ap_at]['analysis_template']
        template_info = ap_at_mapping['analysis_templates'][str(template)]
        mem = basic_memory_routing_by_sequnit(file_names)
#        if template == 3:#metat
#            rqc_q_id="18"
#        elif template ==4: #low input viral
#            rqc_q_id="44"
#        else:#metag
#            mem = basic_memory_routing_by_sequnit(file_names)
#            import pdb;pdb.set_trace()
#            if mem=="120":
#                rqc_q_id=13
#            elif mem=="250":
#                rqc_q_id=41
#            elif mem=="500":
#                rqc_q_id=42
#            elif mem=="1000":
#                rqc_q_id=43
#            elif mem=='see me':
#                rqc_q_id=0
#            else:
#                rqc_q_id=-1
#            
#        rqc_q_name = ap_at_mapping['rqc_queues'][str(rqc_q_id)]
    else:
        sys.stderr.write(ap_at + " is not recognized")
    if debug:
        print(json.dumps(ap_at,indent=3))
    info = hasher()
#    info['rqc_q_id']=rqc_q_id
#    info['rqc_q_name']=rqc_q_name
    info['rqc_mem']=mem
    info['templates']=template_info
    return info

def sequnit_to_pi_info(sequnit,debug=False):
    '''Takes a sequnit and returns proposal/pi info'''
    project_info = dict()
    air = web_services('air', 'su', sequnit, debug=debug)
    if(len(air)>1):
        print("more than one record for air")
    else:
        sequencing_project_name=air[0]['SP_PROJECT_NAME']
        proposal_title=air[0]['PROPOSAL_TITLE']
        proposal_id=air[0]['PROPOSAL_ID']
        proposal_pi=air[0]['PROPOSAL_PI']
        proposal_pi_email=air[0]['PROPOSAL_PI_EMAIL']
        project_info = { 
            'sequencing_project_name'   : sequencing_project_name,
            'proposal_title'            : proposal_title,
            'proposal_id'               : proposal_id,
            'pi_name'                   : proposal_pi,
            'pi_email'                  : proposal_pi_email }

    #with open("out.json","w") as f:f.write(json.dumps(air,indent=3));exit()
    return project_info

def basic_memory_routing_by_sequnit(sequnit,debug=False):
    '''Basic routing, takes input files and returns approx memory'''
    mem = ""
    if len(sequnit)>1:
        mem = "250"
    else:
        rqc_dump = web_services('rqc', 'su', sequnit[0], debug=debug)
        #with open("out","w") as f:f.write(json.dumps(rqc_dump,indent=3));exit()
        #.stats."filter:contamtrimunique31mers_num"
        unique_31_mer = rqc_dump['stats']['filter:contamtrimunique31mers_num']
        unique_31_mer = unique_31_mer/1000000000
        if unique_31_mer <5:
            mem="120"
        elif unique_31_mer <10:
            mem="250"
        elif unique_31_mer <20:
            mem="500"
        else:
            mem="see me"
    return mem 

def metadata_record_to_report_header(meta):
    '''Takes a metadata record and returns a dict of {"report_header":"full_text\n","subtitle":"subtitle only"}'''

    header_text = ""
    title_text = ""
    ap_text = ""
    spid_text = ""
    proposal_text = ""   
    
    if meta['metadata']['analysis_project_id'] and meta['metadata']['analysis_task_id'] :
        apid = meta['metadata']['analysis_project_id'][0]
        atid = meta['metadata']['analysis_task_id'][0]
        ap_info=web_services('pmo','ap', apid)['uss_analysis_project']
        title_text =  "of: " + ap_info['analysis_product_type_name'] +  " - " + ap_info['analysis_project_name']
        ap_text = "Analysis Project/Task ID: " + str(apid) + "/" + str(atid) +  "\n"
    title_text = "JGI assembly " + title_text + ", ASSEMBLY_DATE=" +  time.strftime("%Y%m%d",time.localtime()) 
        
    if len(meta['metadata']['seq_unit_name']) >0 and len(meta['metadata']['sequencing_project_id']) >0:
        spid_text += "Sequencing Project ID(s): " + ", ".join([str(s) for s in meta['metadata']['sequencing_project_id']]) + "\n"
        prop = sequnit_to_pi_info(meta['metadata']['seq_unit_name'][0])
        if set(['pi_name','pi_email','proposal_title','proposal_id']).issubset(list(prop.keys())):# all keys are present 
            meta['metadata'].update({"proposal":prop})
            
            #proposal_text += "Proposal Name: " + 
            proposal_text += "Proposal: " + str(prop['proposal_id']) + " - " + "\n".join(textwrap.wrap(prop['proposal_title'] + "\n", width=80, subsequent_indent='\t\t')) + "\n"
             
            proposal_text += "Principal Investigator: " + prop['pi_name']  + " - " + prop['pi_email'] + "\n"

    header_text += title_text + "\n\n"
    header_text += proposal_text
    header_text += ap_text
    header_text += spid_text
    header_text += "\n"
    return({"report_header":header_text, "subtitle": title_text})

def sequnit_to_lib_spid(sequnit, debug=False):
    '''Takes a jgi sequnit and returns a dict with:
    { library_name: [ CAPAG ], sequencing_project_id: [ 1157300 ], seq_unit_name: [ 12774.2.284685.AGGCAGAA-AAGGAGTA.fastq.gz ] }
    '''
    record = dict()
    jamo_records = web_services("jamo","json",{"file_name":sequnit,"metadata.fastq_type":"sdm_normal",}, debug=debug)
    if debug:
        print(json.dumps(jamo_records,indent=3))
    if len(jamo_records) !=1:
        sys.stderr.write("Check record for " + sequnit)
    else:
        record['library_name']=jamo_records[0]['metadata'].get('library_name','NA')
        record['sequencing_project_id']=jamo_records[0]['metadata']['sow_segment'].get('sequencing_project_id','NA')
        record['seq_unit_name']=[sequnit]
       
    return record

def sequnit_to_rqc_status(sequnit, debug=False):
    '''takes a sequnit and returns rqc usable status
    1 = usable
    0 = not_usable
    -1 = not_found
    '''
    usable = -1
    jamo_records = web_services("jamo","json",{"file_name":sequnit,"metadata.fastq_type":"sdm_normal"}, debug=debug)
    if debug:
        print(json.dumps(jamo_records,indent=3))
    if len(jamo_records) !=1:
        sys.stderr.write("Check record for " + sequnit)
    else:
        if 'metadata' in jamo_records[0] and 'rqc' in jamo_records[0]['metadata'] and 'usable' in jamo_records[0]['metadata']['rqc']:
            if jamo_records[0]['metadata']['rqc']['usable']==False:
                usable = 0
            elif jamo_records[0]['metadata']['rqc']['usable']==True:
                usable = 1
                
    return usable

def its_ap_to_taxonoid(apid):
    '''Takes an apid and returns taxonoid, -1 if not found'''
    data = {}
    try:
        data = web_services('gold', 'ap', apid)
    except:
        pass
    oid = -1
    if "imgTaxonOid" in data:
        oid = data["imgTaxonOid"]
    return oid

def run_krona(raw,directory):
    '''takes a raw reads file and runs krona'''
    
    if not os.path.exists(raw):
        sys.stderr.write("can't find " + raw)
        sys.exit()
    krona_exe = "source /global/projectb/sandbox/rqc/qc_user/miniconda/miniconda2/etc/profile.d/conda.sh && "
    krona_exe += "conda activate qaqc2 && cd " + directory + ' && ' 
    krona_exe += 'python2 /global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/tools/metagenome_krona.py'
    silva = "/global/projectb/sandbox/gaag/bbtools/silva/132/both_deduped_sorted_no_unclassified_bacteria.fa.gz"
    krona_cmd = krona_exe + " " + raw + " " + silva
    print(krona_cmd)
    
    out = subprocess.Popen(krona_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()
    outfiles = [os.path.join(directory, "report.krona." + report_type + '.html') for report_type in ['lsu', 'merged', 'ssu']]
    for filename in outfiles:
        if not(os.path.exists(filename)):
            sys.exit(krona_cmd + " not successful\n" + str(out))
    return outfiles
    
    
def web_services(service, api, data={}, debug=False):
    '''
    Given service, api, and data, return appropriate webservice call results
    args:
       service: {"jamo", "pmo", "rqc", "air", "rqc_readqc_report", "gold"}
       api: {"ap", "at", "fd", "spid", "su", "json", "taxonoid"}
       data: data structure or other data for query example {metadata.sequencing_project_id: 1111111}
    '''
    valid_service = ["jamo","pmo", "rqc_readqc_report", "rqc", "air", "gold"]
    if service not in valid_service:
        raise ValueError("results: service " + service + " must be one of %r." % valid_service)

    valid_api = ["ap","at","fd","spid","su","json", "taxonoid"]
    if api not in valid_api:
        raise ValueError("results: api " + str(api) + " must be one of %r." % valid_api)
    
    urls = dict()
    urls['jamo'] = 'https://sdm2.jgi-psf.org'
    urls['pmo'] = 'https://proposals.jgi.doe.gov/pmo_webservices'
    urls['rqc'] = 'https://rqc.jgi-psf.org'
    urls['air'] = 'https://prospero.jgi.doe.gov'
    urls['gold'] = "https://gold.jgi.doe.gov/rest"
    #https://gold.jgi.doe.gov/rest/img_taxon/3300024346
    headers = dict()
    
    if service == "jamo" and api =="json" and isinstance(data, dict):
        headers={'content-type': 'application/json'}
        url = os.path.join(urls['jamo'],'api/metadata/query')
    elif service == "gold" and api in["ap","taxonoid"]:
        auth_key = "amdpX2dhZzozSzgqZiV6"
        headers = {"Content-Type":"application/json", "Authorization":"Basic " + auth_key}
        if api=="ap":
            url = os.path.join(urls['gold'],'analysis_project',str(data))
        elif api=="taxonoid":
            url = os.path.join(urls['gold'],'img_taxon',str(data))
        data = {}
    elif service == "pmo" and api == "ap":
        url = os.path.join(urls['pmo'],'analysis_project',str(data))
    elif service == "pmo" and api == "at":
        url = os.path.join(urls['pmo'],'analysis_task',str(data))
    elif service == "pmo" and api == "fd":
        url = os.path.join(urls['pmo'],'final_deliverable_project',str(data))        
    elif service == "pmo" and api == "spid":
        url = os.path.join(urls['pmo'],'analysis_projects?sequencing_project_id='+str(data))
    elif service == "air" and api == "su":
        url = os.path.join(urls['air'],"ws/all-inclusive/do?search=sdm_seq_unit_file_name=="+str(data))
    elif service == "rqc" and api == "su":
        url = os.path.join(urls['rqc'],'api/rqcws/dump',str(data))
    elif service == "rqc_readqc_report" and api == "su":
        url = os.path.join(urls['rqc'],'api/readqc/report',str(data))
    else:
        sys.exit("Error: service:" + service + " api:" + api + " combination not implemented")

    if debug:
        sys.stderr.write("url:" + url +  " service=:" + service + " apikey:" + api + "\n")
    
    if headers:
        if data:
            req = urllib.request.Request(url, data=json.dumps(data).encode('utf8'),headers=headers)
        else:
            req = urllib.request.Request(url, headers=headers)
            
    else:
        req = urllib.request.Request(url)
    request = urllib.request.urlopen(req)
    try:
        response = request.read()
    except:
        sys.exit("Error: " + url + " " + data)
    response = json.loads(response)
    return response

def web_services_test():
    '''webservice test for jamo, rqc, its, air, pmo'''
    #happy metagenome
    full_path="/global/dna/dm_archive/rqc/analyses/AUTO-137286/12074.1.235970.AGCTAAC-GGTTAGC.filter-METAGENOME.fastq.gz"
    filepath = os.path.dirname(full_path)
    filename = os.path.basename(full_path)
    j = list()
    #j.append(web_services("gold","ap",106416799, debug=True))
    j.append(web_services("gold","ap",1187780, debug=True))
    j.append(web_services("gold","taxonoid",3300024346, debug=True))
    j.append(web_services("pmo","fd",1150472, debug=True))
    j.append(web_services("jamo","json",{"file_name":filename,"file_path":filepath}, debug=True))
    j.append(web_services("pmo","ap",1150473, debug=True))
    j.append(web_services("pmo","at",195128, debug=True))
    j.append(web_services("pmo","spid",1150504, debug=True))
    j.append(web_services("air","su","12074.1.235970.AGCTAAC-GGTTAGC.fastq.gz", debug=True))
    j.append(web_services("rqc","su","12074.1.235970.AGCTAAC-GGTTAGC.fastq.gz", debug=True))
    j.append(web_services("rqc_readqc_report","su","12074.1.235970.AGCTAAC-GGTTAGC.fastq.gz", debug=True))
#    j.append(web_services("rqc_readqc_report","spid","12074.1.235970.AGCTAAC-GGTTAGC.fastq.gz", debug=True))
    return j

'''Other general utilities'''
    
def get_glob(glob_txt, latest=False):
    '''
    Given a glob string, return file
    args: glob string, and flag to return the newest match
    returns: filenam or None if not found
    '''
    globs = glob.glob(glob_txt)
    globs.sort(key=os.path.getmtime)
    if len(globs) > 1 and latest:
        return globs[-1]
    elif len(globs) > 1:
        sys.stdout.write("Multiple " + glob_txt)
        return None
    elif len(globs) < 1:
        sys.stdout.write("No " + glob_txt)
        return None
    else:
        return globs[0]

def send_mail(**kwargs):
    '''
       sendmail
       keyword arguments:
       (subject, body, sender, reply_to, recipients, attachments)
       '''
    success = False
    #msg = email.MIMEMultipart.MIMEMultipart()
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders
    from email import utils
    import smtplib
    msg = MIMEMultipart('alternative')
    #msg = email.message.EmailMessage()
    msg['From'] = kwargs['sender'] 
    msg['To'] = utils.COMMASPACE.join(kwargs['recipients'])
    msg['Date'] = utils.formatdate(localtime=True)
    msg['Subject'] = kwargs['subject']
    #msg.attach( email.MIMEText.MIMEText(kwargs['body'],'html') )
    msg.attach( MIMEText(kwargs['body'],'html') )
    if 'attachments' in kwargs.keys():
        files = kwargs['attachments']
    else:
        files = []
    for file in files:
        #part = email.MIMEBase.MIMEBase('application', "octet-stream")
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        #email.Encoders.encode_base64(part)
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                     % os.path.basename(file))
        msg.attach(part)

    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(kwargs['sender'], kwargs['recipients'], msg.as_string())
        success = True
    except smtplib.SMTPException:
        success = False

    return success
 

    
def methods_format_wrap(text):
    '''strips, textwraps to 90 and joins on newline'''
    return "\n".join(textwrap.wrap(text.rstrip(), width=90, subsequent_indent='')) + "\n"

def run_process(cmd, verbose=False, fail=True):
    cmd_string=""
    if isinstance(cmd,list):
        if verbose:
            sys.stdout.write("call:" + " ".join(cmd) + " ")
        out = subprocess.Popen(cmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        cmd_string = " ".join(cmd)
    elif isinstance(cmd,str):
        if verbose:
            sys.stdout.write("call:" + cmd + " ")
        out = subprocess.Popen(cmd,shell=True,stderr=subprocess.STDOUT,stdout=subprocess.PIPE)
        cmd_string = cmd
    else:
        sys.stderr.write("trouble running:" + str(cmd), "\n")
        sys.exit(1)
    comm = out.communicate()
    stdout = comm[0].decode('utf-8') if comm[0] is not None else ""
    stderr = comm[1].decode('utf-8') if comm[1] is not None else ""
    rc = out.returncode
    if rc !=0 and fail:
        sys.stderr.write("Trouble running: \"" + cmd_string + "\"\n")
        sys.stderr.write("Error code:" + str(rc) + "\n")
        sys.exit(rc)
    elif verbose:
        sys.stdout.write(" ... returned: " + str(rc) + "\n")
    return_vals = {"stdout":stdout, "stderr":stderr, "rc": rc, "cmd": cmd}
    return return_vals

def hasher():
    '''supports perl style auto-vivification of arbitry depth dicts'''
    return collections.defaultdict(hasher)

if __name__ == '__main__':
    main()
